/*
Navicat MySQL Data Transfer

Source Server         : aartek
Source Server Version : 50067
Source Host           : localhost:3306
Source Database       : everoffe_offerin

Target Server Type    : MYSQL
Target Server Version : 50067
File Encoding         : 65001

Date: 2015-12-18 19:37:29
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `address`
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL auto_increment,
  `address_first` varchar(250) default NULL,
  `address_second` varchar(250) default NULL,
  `address_third` varchar(250) default NULL,
  `contact_no` varchar(25) default NULL,
  `contact_email` varchar(55) default NULL,
  `pin_code` int(11) default NULL,
  `contact_url` varchar(55) default NULL,
  `city_id` int(11) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  `letitude` varchar(55) default NULL,
  `longitude` varchar(55) default NULL,
  `state_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_address` (`city_id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_address` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO address VALUES ('1', 'Mohan takiz,dhar', 'dhar', '', '9685538949', 'vjmali121@gmail.com', '454001', 'www.everoffer.in', '4', null, null, '22.6012922', '75.3024655', '1');
INSERT INTO address VALUES ('2', 'plot no. 164, Queta Colony, Beside Amul ice-cream Parlor', '', '', '7804814213', 'praveen@gmail.com', '440008', '', '10', null, null, '21.1506530', '79.1294516', '3');

-- ----------------------------
-- Table structure for `city`
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL auto_increment,
  `city_name` varchar(55) default NULL,
  `state_id` int(11) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_city` (`state_id`),
  CONSTRAINT `FK_city` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO city VALUES ('1', 'Indore', '1', null, null);
INSERT INTO city VALUES ('2', 'Bhopal', '1', null, null);
INSERT INTO city VALUES ('3', 'Dewas', '1', null, null);
INSERT INTO city VALUES ('4', 'Dhar', '1', null, null);
INSERT INTO city VALUES ('5', 'Ahmedabad', '2', null, null);
INSERT INTO city VALUES ('6', 'Surat', '2', null, null);
INSERT INTO city VALUES ('7', 'Anand', '2', null, null);
INSERT INTO city VALUES ('8', 'Mumbai', '3', null, null);
INSERT INTO city VALUES ('9', 'Pune', '3', null, null);
INSERT INTO city VALUES ('10', 'Nagpur', '3', null, null);
INSERT INTO city VALUES ('11', 'Thane', '3', null, null);
INSERT INTO city VALUES ('12', 'Amravati', '3', null, null);

-- ----------------------------
-- Table structure for `complementary_code`
-- ----------------------------
DROP TABLE IF EXISTS `complementary_code`;
CREATE TABLE `complementary_code` (
  `id` int(11) NOT NULL auto_increment,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  `complementary_code` varchar(55) default NULL,
  `status` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of complementary_code
-- ----------------------------
INSERT INTO complementary_code VALUES ('1', null, null, '64MX88jp2P', '1');
INSERT INTO complementary_code VALUES ('2', null, null, 'h2oTSZIt23', '0');
INSERT INTO complementary_code VALUES ('3', null, null, '8LI4LV1MfF', '0');
INSERT INTO complementary_code VALUES ('4', null, null, 'iS6B295er7', '0');
INSERT INTO complementary_code VALUES ('5', null, null, 'CBAa5yAip1', '1');
INSERT INTO complementary_code VALUES ('6', null, null, '2rbIyar0mB', '0');
INSERT INTO complementary_code VALUES ('7', null, null, '1Mp8e7JvAO', '0');
INSERT INTO complementary_code VALUES ('8', null, null, '930FZ2oO0r', '0');
INSERT INTO complementary_code VALUES ('9', null, null, 'T9yQbpi7q3', '0');
INSERT INTO complementary_code VALUES ('10', null, null, '5RT3crbLn2', '0');
INSERT INTO complementary_code VALUES ('11', null, null, 'BU68w2njt4', '0');
INSERT INTO complementary_code VALUES ('12', null, null, 'gm314ed0lV', '0');
INSERT INTO complementary_code VALUES ('13', null, null, 'LP1V6h6mZL', '0');
INSERT INTO complementary_code VALUES ('14', null, null, 'Tj9E501wtw', '0');
INSERT INTO complementary_code VALUES ('15', null, null, 'p9DP1sw3BH', '0');
INSERT INTO complementary_code VALUES ('16', null, null, '5fX0487f2n', '0');
INSERT INTO complementary_code VALUES ('17', null, null, 'SSxz617A2V', '0');
INSERT INTO complementary_code VALUES ('18', null, null, 'bMWMfqigGR', '0');
INSERT INTO complementary_code VALUES ('19', null, null, '4pUr38yShH', '0');
INSERT INTO complementary_code VALUES ('20', null, null, 's9ifFb4W8V', '0');
INSERT INTO complementary_code VALUES ('21', null, null, 'e0ADF7e0i2', '0');
INSERT INTO complementary_code VALUES ('22', null, null, '6v90sQkwAz', '0');
INSERT INTO complementary_code VALUES ('23', null, null, 'x208nJi1T7', '0');
INSERT INTO complementary_code VALUES ('24', null, null, 'e9hm9lh670', '0');
INSERT INTO complementary_code VALUES ('25', null, null, 'zp6mwmN4jm', '0');
INSERT INTO complementary_code VALUES ('26', null, null, 'DOA209T3on', '0');
INSERT INTO complementary_code VALUES ('27', null, null, 'jYlhmxt331', '0');
INSERT INTO complementary_code VALUES ('28', null, null, '22zD144CIQ', '0');
INSERT INTO complementary_code VALUES ('29', null, null, '237w29Pplz', '0');
INSERT INTO complementary_code VALUES ('30', null, null, 'A3WN43wyFo', '0');
INSERT INTO complementary_code VALUES ('31', null, null, '6kX6YEaoO7', '0');
INSERT INTO complementary_code VALUES ('32', null, null, 'pW4zPi1XOC', '0');
INSERT INTO complementary_code VALUES ('33', null, null, 'njaMs98559', '0');
INSERT INTO complementary_code VALUES ('34', null, null, 'U8sb26tZ41', '0');
INSERT INTO complementary_code VALUES ('35', null, null, '3g4i1M2fEy', '0');
INSERT INTO complementary_code VALUES ('36', null, null, 'CPBJ57rdMC', '0');
INSERT INTO complementary_code VALUES ('37', null, null, '54HmaaOv1o', '0');
INSERT INTO complementary_code VALUES ('38', null, null, 'Nnxz5egPO5', '0');
INSERT INTO complementary_code VALUES ('39', null, null, '7zl4wbl1WE', '0');
INSERT INTO complementary_code VALUES ('40', null, null, 'ut4yDvVchU', '0');
INSERT INTO complementary_code VALUES ('41', null, null, 'B54664Z6B5', '0');
INSERT INTO complementary_code VALUES ('42', null, null, 'tLhnGlj5AP', '0');
INSERT INTO complementary_code VALUES ('43', null, null, 'ly3q30CBsC', '0');
INSERT INTO complementary_code VALUES ('44', null, null, '86WeV28eOj', '0');
INSERT INTO complementary_code VALUES ('45', null, null, '3I01yZGM34', '0');
INSERT INTO complementary_code VALUES ('46', null, null, 'mjI4Xq7C7W', '0');
INSERT INTO complementary_code VALUES ('47', null, null, 'WP4G6cjZ7O', '0');
INSERT INTO complementary_code VALUES ('48', null, null, 'gP332I8d6A', '0');
INSERT INTO complementary_code VALUES ('49', null, null, 'stm5CeMetN', '0');
INSERT INTO complementary_code VALUES ('50', null, null, '4n56778ztF', '0');
INSERT INTO complementary_code VALUES ('51', null, null, 'yhi657X118', '0');
INSERT INTO complementary_code VALUES ('52', null, null, '5ckIW3T333', '0');
INSERT INTO complementary_code VALUES ('53', null, null, 'zq7C331653', '0');
INSERT INTO complementary_code VALUES ('54', null, null, '448Jz6R5C5', '0');
INSERT INTO complementary_code VALUES ('55', null, null, '1W9qp9ym4j', '0');
INSERT INTO complementary_code VALUES ('56', null, null, 'f9124e0yr4', '0');
INSERT INTO complementary_code VALUES ('57', null, null, 'j43wA1U7B7', '0');
INSERT INTO complementary_code VALUES ('58', null, null, 'wTMkh24O6c', '0');
INSERT INTO complementary_code VALUES ('59', null, null, 'UA44i0NLWU', '0');
INSERT INTO complementary_code VALUES ('60', null, null, 'Q38vSB3rR0', '0');
INSERT INTO complementary_code VALUES ('61', null, null, 'i2d9pH4DjP', '0');
INSERT INTO complementary_code VALUES ('62', null, null, 'wTD5l6Jptq', '0');
INSERT INTO complementary_code VALUES ('63', null, null, 'Ln95S6EUY9', '0');
INSERT INTO complementary_code VALUES ('64', null, null, 'H24AJNV232', '0');
INSERT INTO complementary_code VALUES ('65', null, null, 'LfGhPjnayw', '0');
INSERT INTO complementary_code VALUES ('66', null, null, 'S0Y5skEf27', '0');
INSERT INTO complementary_code VALUES ('67', null, null, '7NDCRy8PkM', '0');
INSERT INTO complementary_code VALUES ('68', null, null, '8D7qcpzXy1', '0');
INSERT INTO complementary_code VALUES ('69', null, null, 'h37SZ94jNo', '0');
INSERT INTO complementary_code VALUES ('70', null, null, '2BbUc9k0wv', '0');
INSERT INTO complementary_code VALUES ('71', null, null, 'spZGot6wdL', '0');
INSERT INTO complementary_code VALUES ('72', null, null, 'n6FVNH7afs', '0');
INSERT INTO complementary_code VALUES ('73', null, null, 'zt7bYMG548', '0');
INSERT INTO complementary_code VALUES ('74', null, null, 'AJ6Vy6BW67', '0');
INSERT INTO complementary_code VALUES ('75', null, null, 'Yo38G7AYB1', '0');
INSERT INTO complementary_code VALUES ('76', null, null, 'Ik6ZajeWQR', '0');
INSERT INTO complementary_code VALUES ('77', null, null, 'Ql7Z1dx1O1', '0');
INSERT INTO complementary_code VALUES ('78', null, null, '177GG0568w', '0');
INSERT INTO complementary_code VALUES ('79', null, null, 'T2tFl58OZ9', '0');
INSERT INTO complementary_code VALUES ('80', null, null, '6Um7MP28bh', '0');
INSERT INTO complementary_code VALUES ('81', null, null, 'f619nXkVs2', '0');
INSERT INTO complementary_code VALUES ('82', null, null, 'TXgWB92J49', '0');
INSERT INTO complementary_code VALUES ('83', null, null, '52c17D6fv5', '0');
INSERT INTO complementary_code VALUES ('84', null, null, 'vcP9dg6h2m', '0');
INSERT INTO complementary_code VALUES ('85', null, null, '5a04JG5Gf1', '0');
INSERT INTO complementary_code VALUES ('86', null, null, 'zOZNw4p9Hx', '0');
INSERT INTO complementary_code VALUES ('87', null, null, 'D9q9H3R5P5', '0');
INSERT INTO complementary_code VALUES ('88', null, null, 'vJUi2BnX25', '0');
INSERT INTO complementary_code VALUES ('89', null, null, 'Z38YiouH0v', '0');
INSERT INTO complementary_code VALUES ('90', null, null, '2DanYkoD5Y', '0');
INSERT INTO complementary_code VALUES ('91', null, null, '3qdYnnMMPs', '0');
INSERT INTO complementary_code VALUES ('92', null, null, 'N8mzY16EY5', '0');
INSERT INTO complementary_code VALUES ('93', null, null, 'Q8jRbF52yr', '0');
INSERT INTO complementary_code VALUES ('94', null, null, 'MdOTmC88E7', '0');
INSERT INTO complementary_code VALUES ('95', null, null, 'a77fy31B78', '0');
INSERT INTO complementary_code VALUES ('96', null, null, '8da21ouw7B', '0');
INSERT INTO complementary_code VALUES ('97', null, null, 'RIX6280188', '0');
INSERT INTO complementary_code VALUES ('98', null, null, 'UEO55EUPb2', '0');
INSERT INTO complementary_code VALUES ('99', null, null, 'V2aiAR14nJ', '0');
INSERT INTO complementary_code VALUES ('100', null, null, '99Ut25L5MF', '0');
INSERT INTO complementary_code VALUES ('101', null, null, 'I23iU7N16t', '0');
INSERT INTO complementary_code VALUES ('102', null, null, '9t0X6tAo69', '0');
INSERT INTO complementary_code VALUES ('103', null, null, 'Ceg8939QWp', '0');
INSERT INTO complementary_code VALUES ('104', null, null, '0DDlq0kyO0', '0');
INSERT INTO complementary_code VALUES ('105', null, null, '0J7vtW82w6', '0');
INSERT INTO complementary_code VALUES ('106', null, null, 'SL19x5M3e6', '0');
INSERT INTO complementary_code VALUES ('107', null, null, '5V9d4M3891', '0');
INSERT INTO complementary_code VALUES ('108', null, null, 'Tmo5Y1sUJI', '0');
INSERT INTO complementary_code VALUES ('109', null, null, 'xmUhkYAyaM', '0');
INSERT INTO complementary_code VALUES ('110', null, null, '6FI4VTiSy9', '0');
INSERT INTO complementary_code VALUES ('111', null, null, 'QT84Zk7HCh', '0');
INSERT INTO complementary_code VALUES ('112', null, null, '0b46066g0z', '0');
INSERT INTO complementary_code VALUES ('113', null, null, 'Re934200e2', '0');
INSERT INTO complementary_code VALUES ('114', null, null, 'v15usAG9DA', '0');
INSERT INTO complementary_code VALUES ('115', null, null, 'L4ieLZ6p8w', '0');
INSERT INTO complementary_code VALUES ('116', null, null, '49sW12zWBb', '0');
INSERT INTO complementary_code VALUES ('117', null, null, 'F2O1V9G8L1', '0');
INSERT INTO complementary_code VALUES ('118', null, null, 'cS52g6j7Ri', '0');
INSERT INTO complementary_code VALUES ('119', null, null, 'j8oja99GCH', '0');
INSERT INTO complementary_code VALUES ('120', null, null, 'e4j61v4j5o', '0');
INSERT INTO complementary_code VALUES ('121', null, null, 'f54S12c950', '0');
INSERT INTO complementary_code VALUES ('122', null, null, '2gAb74EMIa', '0');
INSERT INTO complementary_code VALUES ('123', null, null, 'supO1ilX1l', '0');
INSERT INTO complementary_code VALUES ('124', null, null, 'ju941G34q0', '0');
INSERT INTO complementary_code VALUES ('125', null, null, 'DadnFl72MG', '0');
INSERT INTO complementary_code VALUES ('126', null, null, 'n8mQ984D02', '0');
INSERT INTO complementary_code VALUES ('127', null, null, 'e3t702of4S', '0');
INSERT INTO complementary_code VALUES ('128', null, null, 'sAm0eH6caS', '0');
INSERT INTO complementary_code VALUES ('129', null, null, '2zatVoB0HN', '0');
INSERT INTO complementary_code VALUES ('130', null, null, 'SlZh04u6jM', '0');
INSERT INTO complementary_code VALUES ('131', null, null, 'ccj752w430', '0');
INSERT INTO complementary_code VALUES ('132', null, null, 'h7US7GH1CY', '0');
INSERT INTO complementary_code VALUES ('133', null, null, 'Hpk69R3rmA', '0');
INSERT INTO complementary_code VALUES ('134', null, null, '1WOaLnNEz5', '0');
INSERT INTO complementary_code VALUES ('135', null, null, 'JfI2Y4CmO3', '0');
INSERT INTO complementary_code VALUES ('136', null, null, 'x97oWpX5ac', '0');
INSERT INTO complementary_code VALUES ('137', null, null, '2TadrnfC5e', '0');
INSERT INTO complementary_code VALUES ('138', null, null, 'Sw6sb3Az36', '0');
INSERT INTO complementary_code VALUES ('139', null, null, '0D2kmtUJRh', '0');
INSERT INTO complementary_code VALUES ('140', null, null, '43v06nCPZW', '0');
INSERT INTO complementary_code VALUES ('141', null, null, 'FfrPLd4CuY', '0');
INSERT INTO complementary_code VALUES ('142', null, null, 'l2fo2makE9', '0');
INSERT INTO complementary_code VALUES ('143', null, null, 'kLgLn0Bht8', '0');
INSERT INTO complementary_code VALUES ('144', null, null, '9Rz8z7D4ga', '0');
INSERT INTO complementary_code VALUES ('145', null, null, 'N752OQyw89', '0');
INSERT INTO complementary_code VALUES ('146', null, null, '8XGVzDz0ZY', '0');
INSERT INTO complementary_code VALUES ('147', null, null, 'YIjTWC1ykE', '0');
INSERT INTO complementary_code VALUES ('148', null, null, 'lv09N108C8', '0');
INSERT INTO complementary_code VALUES ('149', null, null, 'vx3h5RutFj', '0');
INSERT INTO complementary_code VALUES ('150', null, null, '25VpTTBVp2', '0');
INSERT INTO complementary_code VALUES ('151', null, null, 'XQFi7O5Dv9', '0');
INSERT INTO complementary_code VALUES ('152', null, null, 'zUwB202voB', '0');
INSERT INTO complementary_code VALUES ('153', null, null, 'ELyyGd5IoP', '0');
INSERT INTO complementary_code VALUES ('154', null, null, 'AJ6fpaUp22', '0');
INSERT INTO complementary_code VALUES ('155', null, null, '6r3c83sl8W', '0');
INSERT INTO complementary_code VALUES ('156', null, null, '86ywC0M3F7', '0');
INSERT INTO complementary_code VALUES ('157', null, null, '5HfeaNHS6v', '0');
INSERT INTO complementary_code VALUES ('158', null, null, 'ha6Y1CPeU8', '0');
INSERT INTO complementary_code VALUES ('159', null, null, '0677362g94', '0');
INSERT INTO complementary_code VALUES ('160', null, null, '0sj1Il740S', '0');
INSERT INTO complementary_code VALUES ('161', null, null, '2CvOGX23si', '0');
INSERT INTO complementary_code VALUES ('162', null, null, 'gv1OkmaP21', '0');
INSERT INTO complementary_code VALUES ('163', null, null, '2cA5hg634i', '0');
INSERT INTO complementary_code VALUES ('164', null, null, '6qIC49GEW3', '0');
INSERT INTO complementary_code VALUES ('165', null, null, '9f9l0AF68d', '0');
INSERT INTO complementary_code VALUES ('166', null, null, '3Edh6lc164', '0');
INSERT INTO complementary_code VALUES ('167', null, null, '8FMM3pH6On', '0');
INSERT INTO complementary_code VALUES ('168', null, null, 'JaRZs2e3z7', '0');
INSERT INTO complementary_code VALUES ('169', null, null, 'OSz74Ei45j', '0');
INSERT INTO complementary_code VALUES ('170', null, null, 'zXi2UBC6Uh', '1');
INSERT INTO complementary_code VALUES ('171', null, null, '6wGV59O298', '0');
INSERT INTO complementary_code VALUES ('172', null, null, 'Z704Mf7m8n', '0');
INSERT INTO complementary_code VALUES ('173', null, null, 'Jd0zmZIMPz', '0');
INSERT INTO complementary_code VALUES ('174', null, null, '4Y818t6HlV', '0');
INSERT INTO complementary_code VALUES ('175', null, null, '873Awr7E6e', '0');
INSERT INTO complementary_code VALUES ('176', null, null, '7nPP4LYJF1', '0');
INSERT INTO complementary_code VALUES ('177', null, null, 'VX7yj1OXpY', '0');
INSERT INTO complementary_code VALUES ('178', null, null, 'tbbXt2Ap97', '0');
INSERT INTO complementary_code VALUES ('179', null, null, 'b0o01y9g2o', '0');
INSERT INTO complementary_code VALUES ('180', null, null, 'q1oShMr128', '0');
INSERT INTO complementary_code VALUES ('181', null, null, 'U6yRgv434L', '0');
INSERT INTO complementary_code VALUES ('182', null, null, 'afLg5Va3Ls', '0');
INSERT INTO complementary_code VALUES ('183', null, null, '7bQCg3V6lL', '0');
INSERT INTO complementary_code VALUES ('184', null, null, '6n2QrI3m4L', '0');
INSERT INTO complementary_code VALUES ('185', null, null, 'W7OUReMp9r', '0');
INSERT INTO complementary_code VALUES ('186', null, null, 'X6w589o61V', '0');
INSERT INTO complementary_code VALUES ('187', null, null, '1zY8M7t5Bz', '0');
INSERT INTO complementary_code VALUES ('188', null, null, 'jPeG12808I', '0');
INSERT INTO complementary_code VALUES ('189', null, null, '9V57Hhk4Z1', '0');
INSERT INTO complementary_code VALUES ('190', null, null, 'SEUBUHw6tF', '0');
INSERT INTO complementary_code VALUES ('191', null, null, 'a175zh6fXG', '0');
INSERT INTO complementary_code VALUES ('192', null, null, 'jB5SbvA904', '0');
INSERT INTO complementary_code VALUES ('193', null, null, '8vwvuwPlHY', '0');
INSERT INTO complementary_code VALUES ('194', null, null, '9RuX9HvbA0', '0');
INSERT INTO complementary_code VALUES ('195', null, null, 'lt6kUg0RE0', '0');
INSERT INTO complementary_code VALUES ('196', null, null, '3c8YT6z5m2', '0');
INSERT INTO complementary_code VALUES ('197', null, null, '21i90W903N', '0');
INSERT INTO complementary_code VALUES ('198', null, null, 'pHc3II0p85', '0');
INSERT INTO complementary_code VALUES ('199', null, null, 'H3q3712L4N', '0');
INSERT INTO complementary_code VALUES ('200', null, null, 'ARY772HgDL', '0');

-- ----------------------------
-- Table structure for `contact_us`
-- ----------------------------
DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id` int(55) NOT NULL auto_increment,
  `name` varchar(60) default NULL,
  `email` varchar(30) default NULL,
  `message` varchar(250) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contact_us
-- ----------------------------
INSERT INTO contact_us VALUES ('1', 'Vijay mali', 'vjmali121@gmail.com', 'Hello......', null, null);

-- ----------------------------
-- Table structure for `country`
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL auto_increment,
  `country_name` varchar(55) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO country VALUES ('1', 'India', null, null);

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(55) default NULL,
  `password` varchar(55) default NULL,
  `name` varchar(55) default NULL,
  `merchant_type` int(5) default NULL,
  `opening_time` varchar(55) default NULL,
  `closing_time` varchar(55) default NULL,
  `role_id` int(11) default NULL,
  `address_id` int(11) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  `term_condition` varchar(5) default NULL,
  `time_period` varchar(55) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_login_roleId` (`role_id`),
  KEY `FK_login_addressId` (`address_id`),
  CONSTRAINT `FK_login_addressId` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_login_roleId` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO login VALUES ('1', 'vijayMali', '123456', 'Vijay Mali', '1', '10 : 00 AM', '7 : 00 PM', '2', '1', '2015-12-18 19:16:01', '2015-12-18 19:16:01', 'Yes', '2016-01-17 19:10:42');
INSERT INTO login VALUES ('2', 'sandwich', '123456', 'Hot Sandwiches', '1', '10 : 25 AM', '7 : 25 PM', '2', '2', 'Fri Dec 18 19:27:20 IST 2015', 'Fri Dec 18 19:27:20 IST 2015', 'Yes', '2016-01-17 19:27:20');
INSERT INTO login VALUES ('3', 'admin', '123456', 'Admin', null, null, null, '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for `offer`
-- ----------------------------
DROP TABLE IF EXISTS `offer`;
CREATE TABLE `offer` (
  `id` int(11) NOT NULL auto_increment,
  `offer_description` varchar(250) default NULL,
  `offer_start_time` varchar(55) default NULL,
  `offer_end_time` varchar(55) default NULL,
  `offer_coupan_code` varchar(55) default NULL,
  `offer_heading` varchar(250) default NULL,
  `is_approved` tinyint(4) default NULL,
  `login_id` int(11) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  `offer_image` varchar(55) default NULL,
  `offer_validity` timestamp NULL default NULL,
  `terms_and_conditions` varchar(250) default NULL,
  `no_of_coupons_per_person` int(50) default NULL,
  `image` varchar(100) default NULL,
  `normal_price` int(11) default NULL,
  `offer_price` int(11) default NULL,
  `rate_type` int(11) default NULL,
  `rate_value` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_offer_loginId` (`login_id`),
  CONSTRAINT `FK_offer_loginId` FOREIGN KEY (`login_id`) REFERENCES `login` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of offer
-- ----------------------------
INSERT INTO offer VALUES ('1', 'This is a christmas offer', '12 : 00 AM', '11 : 59 PM', 'WXS2T0', 'By one get one', '1', '1', '2015-12-18 19:16:01', '2015-12-18 19:16:01', null, '2015-12-25 00:00:00', 'No terms and conditons', '2', '0FE95N_pizza12.png', '100', '90', '1', '10');
INSERT INTO offer VALUES ('2', '50 INR is OFF on 200 INR Purchase', '10 : 28 AM', '7 : 28 PM', '2MIZE9', 'Hot Sandwiches  ', '1', '2', 'Fri Dec 18 19:29:41 IST 2015', 'Fri Dec 18 19:29:41 IST 2015', null, '2015-12-28 00:00:00', 'This offer is on the purchase of 250 food items from the store.', '5', 'UWMN74_Lighthouse.jpg', '12', '10', '1', '10');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `role_type` varchar(55) default NULL,
  `created_date` varchar(55) default NULL,
  `updated_date` varchar(55) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO role VALUES ('1', 'Admin', null, null);
INSERT INTO role VALUES ('2', 'Merchant', null, null);
INSERT INTO role VALUES ('3', 'Customer', null, null);

-- ----------------------------
-- Table structure for `state`
-- ----------------------------
DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL auto_increment,
  `state_name` varchar(55) default NULL,
  `country_id` int(11) default NULL,
  `updated_date` varchar(55) default NULL,
  `created_date` varchar(55) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_state` (`country_id`),
  CONSTRAINT `FK_state` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of state
-- ----------------------------
INSERT INTO state VALUES ('1', 'Madhya Pradesh', '1', null, null);
INSERT INTO state VALUES ('2', 'Gujarat', '1', null, null);
INSERT INTO state VALUES ('3', 'Maharashtra', '1', null, null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(50) NOT NULL auto_increment,
  `USER_NAME` varchar(50) default NULL,
  `PASSWORD` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
