package com.ever.offer.in.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ever.offer.in.model.Login;

@Repository
public interface LoginRepository extends JpaRepository<Login, Integer> {

    @Query("select login from Login login where login.employee.id =:employeeId AND login.createdDate BETWEEN :startDate AND :endDate")
    public List<Login> getEmployeeReportService(@Param("employeeId") int employeeId,
                    @Param("startDate") String startDate, @Param("endDate") String endDate);

    /**
     * find by employee id
     * 
     * @param employeeId
     * @return
     */
    List<Login> findByEmployeeId(Integer employeeId);
}
