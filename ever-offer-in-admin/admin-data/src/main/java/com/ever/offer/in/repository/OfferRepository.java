package com.ever.offer.in.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ever.offer.in.model.Offer;

public interface OfferRepository extends JpaRepository<Offer, Integer> {

    /**
     * find by login id
     * 
     * @param loginId
     * @return
     */
    @Query("select offer from Offer offer where offer.login.id=:id")
    public List<Offer> getOfferList(@Param("id") Integer id);

}
