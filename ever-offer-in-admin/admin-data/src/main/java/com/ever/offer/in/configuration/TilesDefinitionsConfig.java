package com.ever.offer.in.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.tiles.Attribute;
import org.apache.tiles.Definition;
import org.apache.tiles.definition.DefinitionsFactory;

public final class TilesDefinitionsConfig implements DefinitionsFactory {
	private static final Map<String, Definition> tilesDefinitions = new HashMap<String, Definition>();
	private static final Attribute BASE_TEMPLATE = new Attribute("/WEB-INF/layout/defaultLayout.jsp");

	public Definition getDefinition(String name, org.apache.tiles.request.Request tilesContext) {
		return tilesDefinitions.get(name);
	}

	/**
	 * @param name
	 *            <code>Name of the view</code>
	 * 
	 * @param title
	 *            <code>Page title</code>
	 * @param body
	 *            <code>Body JSP file path</code>
	 * 
	 *            <code>Adds default layout definitions</code>
	 */
	private static void addDefaultLayoutDef(String name, String title, String body, String header) {
		Map<String, Attribute> attributes = new HashMap<String, Attribute>();

		attributes.put("title", new Attribute("Everoffer.in/Get offer every where"));
		attributes.put("header", new Attribute(header));
		attributes.put("body", new Attribute(body));
		attributes.put("footer", new Attribute("/WEB-INF/layout/footer.jsp"));
		tilesDefinitions.put(name, new Definition(name, BASE_TEMPLATE, attributes));
	}

	public static void addDefinitions() {

		addDefaultLayoutDef("welcome", "welcome", "/WEB-INF/pages/welcome.jsp", "/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("login", "login", "/WEB-INF/pages/login.jsp", "/WEB-INF/layout/header.jsp");
		addDefaultLayoutDef("viewDetailsList", "viewDetailsList", "/WEB-INF/pages/viewOfferDetails.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("randamList", "randamList", "/WEB-INF/pages/viewRandamList.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("viewOfferList", "viewOfferList", "/WEB-INF/pages/viewOfferList.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("merchantList", "merchantList", "/WEB-INF/pages/merchantList.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("managerRegistration", "managerRegistration", "/WEB-INF/pages/managerRegistration.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("supervisorRegistration", "supervisorRegistration",
						"/WEB-INF/pages/supervisorRegistration.jsp", "/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("employeeReport", "employeeReport", "/WEB-INF/pages/employeeReport.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("addAdvertisement", "addAdvertisement", "/WEB-INF/pages/addAdvertisement.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("merchantRegistrationDetails", "merchantRegistrationDetails",
						"/WEB-INF/pages/merchantRegistrationDetails.jsp", "/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("subAdminRegistration", "subAdminRegistration", "/WEB-INF/pages/subAdminRegistration.jsp",
						"/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("addOffer", "addOffer", "/WEB-INF/pages/addOffer.jsp", "/WEB-INF/layout/homeHeader.jsp");
		addDefaultLayoutDef("employeeRegistration", "AddFiledOfficer", "/WEB-INF/pages/employeeRegistration.jsp", "/WEB-INF/layout/homeHeader.jsp");
	}

}
