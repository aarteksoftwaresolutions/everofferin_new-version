package com.ever.offer.in.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ever.offer.in.model.ComplementaryCode;

public interface ComplementaryCodeRepository extends JpaRepository<ComplementaryCode, Integer> {

}
