package com.ever.offer.in.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ever.offer.in.model.Employee;

@Repository
public interface GenerateReportRepository extends JpaRepository<Employee, Integer> {

}
