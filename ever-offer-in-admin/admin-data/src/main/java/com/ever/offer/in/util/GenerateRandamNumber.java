package com.ever.offer.in.util;

import java.util.Random;

public class GenerateRandamNumber {
	public static String getRandomNumber(int length) {
		final String characters = "1234567890abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
		StringBuilder result = new StringBuilder();
		while (length > 0) {
			Random rand = new Random();
			result.append(characters.charAt(rand.nextInt(characters.length())));
			length--;
		}
		String randamNumber = result.toString();
		return randamNumber;

	}
}
