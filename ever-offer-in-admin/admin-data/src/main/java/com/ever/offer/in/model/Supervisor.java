package com.ever.offer.in.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "supervisor")
public class Supervisor extends AbstractEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "emai")
	private String email;

	@Column(name = "contact")
	private String contact;

	@Column(name = "address")
	private String address;

	private ManagerRegistration managerRegstration;

	private Integer isDeleted;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manager_id")
	public ManagerRegistration getManagerRegstration() {
		return managerRegstration;
	}

	public void setManagerRegstration(ManagerRegistration managerRegstration) {
		this.managerRegstration = managerRegstration;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "is_deleted")
	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

}
