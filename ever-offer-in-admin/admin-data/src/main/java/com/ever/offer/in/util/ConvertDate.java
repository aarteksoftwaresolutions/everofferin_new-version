package com.ever.offer.in.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertDate {
	public static String getConvertedDate(String date) {
		String finalDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date dateNew = dateFormat.parse(date);
			finalDate = dateFormat1.format(dateNew);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return finalDate;
	}
	
	public static String getDate(){
		  Date date=new Date();
		  SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
		
	}
}
