package com.ever.offer.in.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ever.offer.in.model.City;

public interface CityRepository extends JpaRepository<City, Integer> {

}
