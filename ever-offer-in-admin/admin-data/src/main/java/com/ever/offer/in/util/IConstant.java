package com.ever.offer.in.util;

public class IConstant {

	public static final String INVALID_LOGIN_MESSAGE = "Invalid user name and password";
	public static final int ADMIN_ROLE_ID = 1;
	public static final int MERCHANT_ROLE_ID = 2;
	public static final int RANDAM_NUMBER = 10;
	public static final int STATUS = 0;
	public static final int IS_APPROVED = 1;
	public static final int NOT_APPROVED = 0;
	public static final String LOGOUT_SUCCESSFUL = "Logout successfully";
	public static final String MANAGER = "Marketing officer successfully added.";
	public static final String SUPERVISOR = "Development officer successfully added.";
	public static final String EMPLOYEE = "Field officer successfully added.";
	public static final String SUB_ADMIN = "Registration successful.";
	public static final String UPDATE_DETAILS = "Offer details updated.";
	public static final int IS_DELETED = 1;
	public static final int IS_DELETED_DEACTIVE = 0;
	public static final int ONE = 1;
	public static final int ZERO = 0;
	public static final int TWO = 2;
	
	//public static final String FILE_PATH = "E:/Aartek/images/ads";
	
	public static final String ADVERTISEMENT_IMAGE = "/home/everoffer/appservers/apache-tomcat-7x/webapps/advImage";
	
	public static final String FILE_PATH = "/home/everoffer/appservers/apache-tomcat-7x/webapps/upload";
	
	public static final String ADVERTISEMENT="Advertisement added ";
	/*it is used for login web service*/
	public static final String RESPONSE = "response";
	public static final String DATA = "DATA";
	public static final String MESSAGE  = "MESSAGE";
	public static final String LOGIN_MESSAGE = "Admin logged in";
	public static final String INVALID_LOGIN = "invalid userName or password";
	
	/*** For merchant subscription plan */
	public static final int THIRTY_DAYS_SUBSCRIPTION = 1;
	public static final int THREE_MONTH_SUBSCRIPTION = 2;
	public static final int SIX_MONTHS_SUBSCRIPTION = 3;
	public static final int ONE_YEAR_SUBSCRIPTION = 4;
	
	/*** check subscription plan for combo offer limit */
	public static final int OFFER_LIMIT_OF_COMBO_THIRTY_DAYS_SUBSCRIPTION = 3;
	public static final int OFFER_LIMIT_OF_COMBO_THREE_MONTH_SUBSCRIPTION = 3;
	public static final int OFFER_LIMIT_OF_COMBO_SIX_MONTHS_SUBSCRIPTION = 3;
	public static final int OFFER_LIMIT_OF_COMBO_ONE_YEAR_SUBSCRIPTION = 3;
	
	/*** For merchant subscription plan amount */
	public static final int THIRTY_DAYS_SUBSCRIPTION_SMALL_AMOUNT = 400;
	public static final int THREE_MONTH_SUBSCRIPTION_SMALL_AMOUNT = 900;
	public static final int SIX_MONTHS_SUBSCRIPTION_SMALL_AMOUNT = 1500;
	public static final int ONE_YEAR_SUBSCRIPTION_SMALL_AMOUNT =2500;

	public static final int THIRTY_DAYS_SUBSCRIPTION_COMBO_AMOUNT = 650;
	public static final int THREE_MONTH_SUBSCRIPTION_COMBO_AMOUNT = 1500;
	public static final int SIX_MONTHS_SUBSCRIPTION_COMBO_AMOUNT = 2000;
	public static final int ONE_YEAR_SUBSCRIPTION_COMBO_AMOUNT = 4000;
	
}
