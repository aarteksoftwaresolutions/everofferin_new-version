package com.ever.offer.in.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {
	public static String getformattedDate(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.format(date);
	}

	/**
	 * Method for get simple date format.
	 * 
	 * @return
	 */
	public static String getDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);

	}

	public static void main(String[] args) {
		// System.out.println("method no 4 ="+DateFormatUtil.getDate());
		// System.out.println("get offer validity date=   " +
		// DateFormatUtil.getRemainingDaysForoffer("06-01-2016"));
		// System.out.println("get remaining date=   " +
		// DateFormatUtil.getRemainingDays("04-01-2016"));
	}

}