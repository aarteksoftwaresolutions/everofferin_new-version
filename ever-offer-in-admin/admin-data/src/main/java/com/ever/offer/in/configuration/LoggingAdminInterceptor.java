package com.ever.offer.in.configuration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.ever.offer.in.model.SubAdmin;

/**
 * 
 * @author Meenal Pathre
 *
 */
public class LoggingAdminInterceptor implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		System.out.println("---Before Method Execution---");
		SubAdmin adminLogin = (SubAdmin) request.getSession().getAttribute("subAdmin");
		if (adminLogin == null || adminLogin.equals("")) {
			response.sendRedirect("subAdmin");
			System.out.println("------------Interceptor--------------------");
			return false;
		}

		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
					ModelAndView modelAndView) throws Exception {
		System.out.println("---method executed---");
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
					throws Exception {
		System.out.println("---Request Completed---");
	}
}
