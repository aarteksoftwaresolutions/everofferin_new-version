package com.ever.offer.in.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ever.offer.in.model.SubAdmin;

@Repository
public interface SubAdminRepository extends JpaRepository<SubAdmin, Integer> {

    /**
     * find by userName and password
     * 
     * @param userName
     * @param password
     * @return SubAdmin instance
     */
    SubAdmin findByUserNameAndPassword(String userName, String password);
}
