package com.ever.offer.in.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ever.offer.in.model.Supervisor;

@Repository
public interface SupervisorRepository extends JpaRepository<Supervisor, Integer> {

    /**
     * find by isDeleted
     * 
     * @param isDeleted
     * @return
     */
    List<Supervisor> findByIsDeleted(int isDeleted);

}
