package com.ever.offer.in.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ever.offer.in.model.Login;

public interface AdminRepository extends JpaRepository<Login, Integer> {

    /**
     * find by role id
     * 
     * @param merchantRoleId
     * @return logins
     */
    public List<Login> findByRoleId(Integer adminRoleId);
}
