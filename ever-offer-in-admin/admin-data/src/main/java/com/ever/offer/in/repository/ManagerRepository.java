package com.ever.offer.in.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ever.offer.in.model.ManagerRegistration;

@Repository
@Transactional
public interface ManagerRepository extends JpaRepository<ManagerRegistration, Integer> {

    /**
     * find by isDeleted
     * 
     * @param isDeleted
     * @return
     */
    List<ManagerRegistration> findByIsDeleted(int isDeleted);

}
