var geocoder;
var map;
var marker;
var infowindow = new google.maps.InfoWindow({
	size : new google.maps.Size(150, 50)
});
function initialize() {
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(-34.397, 150.644);
	var mapOptions = {
		zoom : 10,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
	google.maps.event.addListener(map, 'click', function() {
		infowindow.close();
	});
}

function clone(obj) {
	if (obj == null || typeof (obj) != 'object')
		return obj;
	var temp = new obj.constructor();
	for ( var key in obj)
		temp[key] = clone(obj[key]);
	return temp;
}

function geocodePosition(pos, id) {
	geocoder
			.geocode(
					{
						latLng : pos
					},
					function(responses) {
						if (responses && responses.length > 0) {
							marker.formatted_address = responses[0].formatted_address;
						} else {
							marker.formatted_address = 'Cannot determine address at this location.';
						}

						infowindow.setContent(marker.formatted_address
								+ "<br>coordinates: "
								+ marker.getPosition().toUrlValue(6));
						if (confirm("Do you want to save the changes ?")) {
							var latLang = marker.getPosition().toUrlValue(6);
							var a = latLang.split(",");
							$
									.ajax({
										url : "updateLatLang?lat=" + a[0]
												+ "&lang=" + a[1]
												+ "&merchantId=" + id,
										type : "GET",
										contentType : "application/json; charset=utf-8",
										success : function(call) {
											location.reload();
										},
										error : function() {
											alert("wrong");
										}
									});

						} else {
							alert("Something went to wrong");
						}
						infowindow.open(map, marker);
					});
}

function geocodePosition1(pos) {
	geocoder
			.geocode(
					{
						latLng : pos
					},
					function(responses) {
						if (responses && responses.length > 0) {
							marker.formatted_address = responses[0].formatted_address;
						} else {
							marker.formatted_address = 'Cannot determine address at this location.';
						}

						infowindow.setContent(marker.formatted_address
								+ "<br>coordinates: "
								+ marker.getPosition().toUrlValue(6));
						infowindow.open(map, marker);
					});
}

function codeAddress(lat, lng, id) {
	var latlng = new google.maps.LatLng(lat, lng);
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({
		'latLng' : latlng
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			if (marker) {
				marker.setMap(null);
				if (infowindow)
					infowindow.close();
			}
			marker = new google.maps.Marker({
				map : map,
				draggable : true,
				position : results[0].geometry.location
			});
			google.maps.event.addListener(marker, 'dragend', function() {
				geocodePosition(marker.getPosition(), id);
			});
			google.maps.event.addListener(marker, 'click', function() {
				if (marker.formatted_address) {
					geocodePosition1(marker.getPosition());
				} else {
					geocodePosition1(marker.getPosition());
				}
				infowindow.open(map, marker);
			});
			google.maps.event.trigger(marker, 'click');
		} else {
			alert('Geocode was not successful for the following reason: '
					+ status);
		}
	});
}

function showMap(lat, lng, id){
	document.getElementById('map_canvas').style.visibility = 'visible';
	codeAddress(lat, lng, id);
}
