
	function enableOfferStatus(status) {
		var statusId = status.id;
		var statusValue = status.value;
		var statusIdValue = document.getElementById(statusId).checked;
		$.ajax({
			url : "offerStatus?statusValue=" + statusValue + "&statusIdValue="
					+ statusIdValue,
			type : "GET",
			contentType : "application/json; charset=utf-8",
			success : function(call) {
			},
			error : function() {
			}
		})
	}
	
	function enableAdStatus(status) {
		var statusId = status.id;
		var statusValue = status.value;
		var statusIdValue = document.getElementById(statusId).checked;
		$.ajax({
			url : "changeAdStatus?statusValue=" + statusValue + "&statusIdValue="
					+ statusIdValue,
			type : "GET",
			contentType : "application/json; charset=utf-8",
			success : function(call) {
			},
			error : function() {
			}
		})
	}
