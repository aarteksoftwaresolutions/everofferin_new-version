<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="java">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>IT Jobbers</title>

<!-- Bootstrap Core CSS -->
<link href="resources/css/admin_bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="resources/css/admin_sb-admin.css" rel="stylesheet">
<link href="resources/css/admin_form.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="resources/css/admin_style.css">


<!-- Custom Fonts -->
<link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="welcome"><img src="resources/images/ITjobbers_Loog.png"></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                        class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview"><a href="#">
                                <div class="media">
                                    <span class="pull-left"> <img class="media-object"
                                        src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            <strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted">
                                            <i class="fa fa-clock-o"></i> Yesterday at 4:32 PM
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                        </a></li>
                        <li class="message-preview"><a href="#">
                                <div class="media">
                                    <span class="pull-left"> <img class="media-object"
                                        src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            <strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted">
                                            <i class="fa fa-clock-o"></i> Yesterday at 4:32 PM
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                        </a></li>
                        <li class="message-preview"><a href="#">
                                <div class="media">
                                    <span class="pull-left"> <img class="media-object"
                                        src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            <strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted">
                                            <i class="fa fa-clock-o"></i> Yesterday at 4:32 PM
                                        </p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                        </a></li>
                        <li class="message-footer"><a href="#">Read All New Messages</a></li>
                    </ul></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                        class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li><a href="#">Alert Name <span class="label label-default">Alert Badge</span></a></li>
                        <li><a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a></li>
                        <li><a href="#">Alert Name <span class="label label-success">Alert Badge</span></a></li>
                        <li><a href="#">Alert Name <span class="label label-info">Alert Badge</span></a></li>
                        <li><a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a></li>
                        <li><a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a></li>
                        <li class="divider"></li>
                        <li><a href="#">View All</a></li>
                    </ul></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                        class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-fw fa-user"></i> Profile</a></li>
                        <li><a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a></li>
                        <li><a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul></li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li><a href="welcome"><i class="fa fa-fw fa-dashboard"></i> Home</a></li>
                    <li><a href="merchantList"><i class="fa fa-fw fa-user"></i> Merchants</a></li>
                    <c:if test="${subAdmin.role.id==1}">
                        <li><a href="subAdminRegistration"><i class="fa fa-fw fa-user"></i> Add Subadmin</a></li>
                        <li><a href="manager"><i class="fa fa-fw fa-user"></i> Add Marketing officer</a></li>
                        <li><a href="supervisor"><i class="fa fa-fw fa-user"></i> Add development officer</a></li>
                        <li><a href="registration"><i class="fa fa-fw fa-user"></i> Add field officer</a></li>
                        <li><a href="generateReport"><i class="fa fa-fw fa-briefcase"></i> Field officer report</a></li>

                        <li><a href="advertisement"><i class="fa fa-fw fa-briefcase"></i> Add advertisement</a></li>
                        <li><a href="generateComplementaryCode"><i class="fa fa-fw fa-briefcase"></i>
                                Complementary code</a></li>
                        <!-- 	<li><a href="javascript:;" data-toggle="collapse"
							data-target="#mail"><i class="fa fa-fw fa-envelope"></i>
								Group mailing<i class="fa fa-fw fa-caret-down"></i></a>
							<ul id="mail" class="collapse">
								<li><a href="#">To marketing officer</a></li>
								<li><a href="#">To development officer</a></li>
								<li><a href="#">To Field officer</a></li>
							</ul></li> -->
                    </c:if>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="resources/js/admin_jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="resources/js/admin_bootstrap.min.js"></script>
</body>

</html>
