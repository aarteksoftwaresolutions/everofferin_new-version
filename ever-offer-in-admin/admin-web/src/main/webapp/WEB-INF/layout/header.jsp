<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="resources/images/favicon.ico" />
<title>EverOffer</title>
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--menu-->
<script src="resources/js/scripts.js"></script>
<link href="resources/css/styles.css" rel="stylesheet">
<!--//menu-->
<!--theme-style-->
<link
	href="https://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600italic,600,700,700italic,800,800italic"
	rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Real Home  Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	
	
	
	
addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 







</script>
</head>
<body>
	<!--header-->
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6 col-xs-6">
					<div class="top-number">
						<p>
							<span class="glyphicon glyphicon-earphone"></span> +0123 456 70
							90
						</p>
					</div>
				</div>
				<div class="col-sm-6 col-xs-6">
					<div class="top-navigation">
						<ul class="top-nav">
							<li><a href="#tf-home" class="page-scroll">frequently ask
									questions</a></li>
						</ul>

					</div>
				</div>
			</div>
		</div>
		<!--/.container-->
	</div>
	<!--/.top-bar-->

	<nav id="tf-menu" class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="login"><img
					src="resources/images/Logo.png"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="navbar-collapse collapse"
				id="bs-example-navbar-collapse-1" aria-expanded="false"
				style="height: 1px;">

				<ul class="nav navbar-nav navbar-right">
					<c:if test="${login== null}">
						<li><a href="login.html" class="page-scroll">Merchant
								login</a></li>
					</c:if>
				</ul>

				<c:if test="${login!= null}">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="addOffer" class="page-scroll">Add Offer</a></li>
						<li><a href="logout.html" class="page-scroll">Merchant
								Logout</a></li>
					</ul>
				</c:if>

			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<!--//-->

	<!--//header-->
</body>
</html>