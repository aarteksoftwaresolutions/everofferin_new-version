<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/css/validationEngine.jquery.css"
	type="text/css" />
<script src="resources/js/jquery.validationEngine-en.js"
	type="text/javascript" charset="utf-8"></script>
<script src="resources/js/jquery.validationEngine.js"
	type="text/javascript" charset="utf-8"></script>
<script src="resources/js/page-js/base64.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript"
	src="resources/js/commonForFormValidation.js"></script>
<script type="text/javascript">
        function convertIdIntoBase64(id) {
            var encodedId = Base64.encode(id.toString());
            document.location.href = "editManager?id="+encodedId;
        }
</script>
</head>
<body>
	<div id="page-wrapper">

		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-8">
					<div id="admincontainer">

						<div>
							<h3 align="center" class="hadding">Marketing Officer
								Registration</h3>
						</div>
						<form:form method="POST" action="saveManager" id="formID"
							modelAttribute="ManagerRegistration" autocomplete="off">
							<form:hidden path="id" />
							<div class="panel-body">
								<c:if test="${empty massage}">
									<div class="alert alert-info">Please fill details to add
										a new Marketing Officer</div>
								</c:if>
								<c:if test="${not empty massage}">
									<div class="alert alert-info">${massage}</div>
								</c:if>
								<div class="forpass_sec form-sec">

									<div class="col-sm-12 frmpadding box">
										<div class="col-lg-3 col-sm-3 ">

											<label>Name</label>
											<form:input path="name" maxlength="60"
												class="validate[required] input-text user form-control"
												placeholder="Name" />

										</div>
										<div class="col-lg-3 col-sm-3 ">

											<label>Email Id</label>
											<form:input path="email" maxlength="50"
												class="validate[required,custom[email],maxSize[50]] input-text user form-control"
												placeholder="Email Id" />
										</div>
										<div class="col-lg-12 col-sm-12 ">

											<label>Contact No.</label>
											<form:input path="contact" maxlength="10"
												class="validate[required,custom[phone],minSize[10],maxSize[10]] input-text user form-control"
												placeholder="Contact No." />

										</div>
										<div class="col-lg-12 col-sm-12">
											<label>Address</label>
											<form:textarea path="address" maxlength="250"
												class="validate[required] input-text form-control" rows="6"
												placeholder="Address " />
										</div>

										<div class="col-sm-3 frmpadding">

											<input type="submit" value="Submit" />
										</div>
									</div>
								</div>
							</div>
						</form:form>

					</div>

				</div>
			</div>
			<div style="margin-top: 26px" class="table-responsive">
				<c:if test="${not empty managerList }">
					<table border="1" style="width: 100%; height: 26px;" id="tableData"
						class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Name</th>
								<th>Email</th>
								<th>Contact</th>
								<th>Address</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="view" items="${managerList}" varStatus="status">
								<tr>
									<td>${status.index+ 1}</td>
									<td>${view.name}</td>
									<td>${view.email}</td>
									<td>${view.contact}</td>
									<td>${view.address}</td>
									<td><a href="#"
										onclick="return convertIdIntoBase64(${view['id']});">Edit
									</a></td>
									<td><a href="deleteManager.do?id=${view.id}"
										onclick="return confirm('Please confirm if you want to delete this Details!');">Delete</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${empty managerList}">
					<h1 align="left" class="msg" style="margin-bottom: 10px;">No Records Found</h1>
				</c:if>

			</div>
		</div>
	</div>
</body>
</html>