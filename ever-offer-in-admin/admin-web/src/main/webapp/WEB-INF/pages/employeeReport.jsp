<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery.datetimepicker.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="resources/js/page-js/base64.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript">
	function convertIdIntoBase64(employeeId) {
		var encodedId = Base64.encode(employeeId.toString());
		document.location.href = "employeeDetails?employeeId=" + encodedId;
	}
</script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#demo2')
								.append('<div></div>')
								.html(
										"Please fill details to generate Field officer's Report");
						var obj2 = document.getElementById('div2');
						obj2.style.display = "true";
					});
	function generateEmployeeReport() {
		var employeeId = $("#category option:selected").val();
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		if (employeeId != 0) {
			$
					.ajax({
						url : "getEmployeeReport?employeeId="
								+ encodeURIComponent(employeeId)
								+ '&startDate=' + encodeURIComponent(startDate)
								+ '&endDate=' + encodeURIComponent(endDate),
						type : "GET",
						contentType : "application/json; charset=utf-8",
						success : function(t) {

							var e = t;
							var showData = e;
							$('#showDataTable').empty();
							if (showData != null && showData != "") {
								$('#demo2').append('<div></div>').html("");
								var obj2 = document.getElementById("div2");
								obj2.style.display = "true";
								$('#showDataTable')
										.append(
												'<table id="testTable" width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-striped"></table>');
								var table = $('#showDataTable').children();
								table
										.append("<thead><tr><td>S. No.</td><td>Field Officer</td><td>Address</td><td>Email</td><td>Development Officer</td><td>Marketing Officer</td><td>Amount</td><td>Total</td><td>Details</td></tr>");
								for (i = 0; i < e.length; i++) {
									var j = 1;
									table
											.append($("<tr><td>"
													+ j++
													+ "</td><td>"
													+ e[0].fieldOfficer
													+ "</td>"
													+ "<td>"
													+ e[0].empAddress
													+ "</td>"
													+ "<td>"
													+ e[0].emailId
													+ "</td><td>"
													+ e[0].supervisior
													+ "</td><td>"
													+ e[0].manager
													+ "</td><td>"
													+ e[0].amount
													+ "</td><td>"
													+ e[0].count
													+ "</td><td>"
													+ '<a href="#" onclick=" convertIdIntoBase64('+ employeeId+')">Details</a>'
													+ "</td></tr></thead>"));
								}
							} else {
								$('#demo2').append('<div></div>').html(
										"No Data Found");
								var obj2 = document.getElementById('div2');
								obj2.style.display = "true";

							}
						},
						error : function() {

						}
					})

		} else {
			$('#demo2').append('<div></div>').html("No Data Found");
			var obj2 = document.getElementById('div2');
			obj2.style.display = "true";
		}

	}
</script>
</head>

<body>

	<div id="page-wrapper">

		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-8">
					<div id="admincontainer">

						<div>
							<h3 align="center" class="hadding">Field officer Report</h3>
						</div>

						<div class="panel-body">

							<div id="div2" class="alert alert-info">
								<p id="demo2"></p>
							</div>
							<div class="forpass_sec form-sec">

								<div class="col-sm-12 frmpadding box">
									<div class="col-lg-3 col-sm-3 ">

										<label>From Date</label> <input type="text"
											class="user form-control" style="margin-right: 100px"
											id="startDate" placeholder="From Date" />

									</div>
									<div class="col-lg-3 col-sm-3 ">

										<label>To Date</label> <input type="text" id="endDate"
											class="user form-control" placeholder="To Date" />
									</div>


									<div class="col-lg-12 col-sm-12 frmpadding">

										<label>Select Field officer</label>
										<form:select path="employee.id" class="dropdown" tabindex="9"
											data-settings="{&quot;wrapperClass&quot;:&quot;flat&quot;}"
											id="category">
											<form:option value="0" label="Select Field Officer Reference" />
											<c:forEach items="${employeeList}" var="empList">
												<form:option value="${empList.id}" label="${empList.name}" />
											</c:forEach>
										</form:select>
									</div>
									<div class="col-sm-3 frmpadding">

										<input type="submit" value="Submit" id="exportButton"
											onclick="return generateEmployeeReport()" />
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="showDataTable" class="table-responsive"></div>

	</div>
</body>
<script src="resources/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	jQuery('#endDate').datetimepicker({
		timepicker : false,
		format : 'd-m-Y'
	});

	jQuery('#startDate').datetimepicker({
		timepicker : false,
		format : 'd-m-Y'
	});
</script>
</html>