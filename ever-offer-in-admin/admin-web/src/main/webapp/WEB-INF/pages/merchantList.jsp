<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="resources/js/page-js/base64.js" type="text/javascript"
	charset="utf-8"></script>
<script src="resources/js/page-js/updateLocation.js"
	type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	function convertIdIntoBase64(id) {
		var encodedId = Base64.encode(id.toString());
		document.location.href = "offerList?id=" + encodedId;
	}
</script>
</head>
<body onload="initialize()">
	<form:form align="center" method="POST" modelAttribute="Login">
		<div id="page-wrapper">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row grid2"></div>
				<!-- /.row -->
				<div class="row">
					<div id="admincontainer" class="col-lg-12">
						<div class="col-lg-12">
							<c:if test="${not empty merchantList}">
								<h1 class="text-left">Merchants</h1>
								<div class="table-responsive grid2 col-lg-12">
									<table class="table table-hover tab table-striped"
										id="tableData">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Merchants</th>
												<th>Name</th>
												<th>Password</th>
												<th>Opening Time</th>
												<th>Closing Time</th>
												<th>Offers</th>
												<th>View In Map</th>
											</tr>
										</thead>
										<c:forEach items="${merchantList}" var="merchantList"
											varStatus="status">
											<tr align="center">
												<td>${status.index+ 1 }</td>
												<td>${merchantList.name}</td>
												<td>${merchantList.userName}</td>
												<td>${merchantList.password}</td>
												<td>${merchantList.openingTime}</td>
												<td>${merchantList.closingTime}<input type="hidden"
													id="merchantId"></td>
												<td><a href="#"
													onclick="return convertIdIntoBase64(${merchantList['id']});">View
														Offer </a></td>
												<td><a href="#" id="address"
													onclick="showMap(${merchantList.address.letitude}, ${merchantList.address.longitude},${merchantList.id});">View
														In Map </a></td>
											</tr>
										</c:forEach>

										</tbody>
									</table>

								</div>
							</c:if>
							<c:if test="${empty merchantList}">
								<h1 align="center" style="color: red">No Records Found</h1>
							</c:if>

							<div id="map_canvas" class="col-lg-12"></div>
							<script src="http://www.google-analytics.com/urchin.js"
								type="text/javascript"> 
</script>
							<script type="text/javascript"> 
_uacct = "UA-162157-1";
urchinTracker();
</script>
							<div class="row"></div>
						</div>
					</div>
				</div>

				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
	</form:form>
</body>
</html>