<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resources/css/validationEngine.jquery.css"
	type="text/css" />
<script src="resources/js/jquery.validationEngine-en.js"
	type="text/javascript" charset="utf-8"></script>
<script src="resources/js/jquery.validationEngine.js"
	type="text/javascript" charset="utf-8"></script>
<script type="text/javascript"
	src="resources/js/commonForFormValidation.js"></script>
</head>
<body>
	<div id="page-wrapper">

		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-8">
					<div id="admincontainer">

						<div>
							<h3 align="center" class="hadding">Sub Admin Registration</h3>
						</div>
						<form:form method="POST" action="saveSubadmin" id="formID"
							modelAttribute="SubAdmin" autocomplete="off">
							<form:hidden path="role.id" value="4" />
							<div class="panel-body">
								<c:if test="${empty massage}">
									<div class="alert alert-info">Please fill details to
										register a new sub admin</div>
								</c:if>
								<c:if test="${not empty massage}">
									<div class="alert alert-info">${massage}</div>
								</c:if>
								<div class="forpass_sec form-sec">

									<div class="col-sm-12 frmpadding box">
										<div class="col-lg-3 col-sm-3 ">

											<label>User Name</label>
											<form:input path="userName" maxlength="16"
												class="validate[required] input-text user form-control"
												placeholder="User Name" />

										</div>
											<div class="col-lg-3 col-sm-3 ">

											<label>Password</label>
											<form:password path="password" maxlength="16"
												class="validate[required,minSize[5],maxSize[12]] input-text"
												placeholder="Password" />

										</div>
										
										<div class="col-lg-3 col-sm-3 ">

											<label>Full Name</label>
											<form:input path="name" maxlength="60"
												class="validate[required] input-text user form-control"
												placeholder="Full Name" />

										</div>
										<div class="col-lg-3 col-sm-3 ">

											<label>Contact No.</label>
											<form:input path="contact" maxlength="10"
												class="validate[required,custom[phone],minSize[10],maxSize[10]] input-text user form-control"
												placeholder="Contact No." />

										</div>
										<div class="col-sm-3 frmpadding">
											<input type="submit" value="Submit" />
										</div>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
				<div style="margin-top: 26px" class="table-responsive">
					<c:if test="${not empty subAdminList }">
						<table border="1" style="width: 100%; height: 26px;"
							id="tableData" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>No.</th>
									<th>Name</th>
									<th>Contact</th>
									<th>User Name</th>
									<th>Password</th>
									<!-- <th>Edit</th>
									<th>Delete</th> -->
								</tr>
							</thead>
							<tbody>
								<c:forEach var="view" items="${subAdminList}" varStatus="status">
									<tr>
										<td>${status.index+ 1}</td>
										<td>${view.name}</td>
										<td>${view.contact}</td>
										<td>${view.userName}</td>
										<td>${view.password}</td>
										<%-- <td><a href="#"
											onclick="return convertIdIntoBase64(${view['id']});">Edit
										</a></td>
										<td><a href="deleteEmployee.do?id=${view.id}"
											onclick="return confirm('Please confirm if you want to delete this Details!');">Delete</a></td> --%>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
					<c:if test="${empty subAdminList}">
						<h1 align="left" class="msg" style="margin-bottom: 10px;">No Records Found</h1>
					</c:if>

				</div>
		</div>
	</div>
</body>
</html>