<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="resources/js/page-js/viewOfferList.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="resources/js/page-js/base64.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript">
        function convertIdIntoBase64(id) {
            var encodedId = Base64.encode(id.toString());
            document.location.href = "offerDetailsList?id="+encodedId;
        }
</script>
</head>
<body>
	<form:form align="center" method="POST" modelAttribute="Offer">
		<div id="page-wrapper">

			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row grid2"></div>
				<!-- /.row -->

				<div class="row">

					<div id="admincontainer" class="col-lg-12">
						<div class="col-lg-12">
							<c:if test="${not empty offerList}">
								<h1 class="text-left">Offers</h1>
								<div class="table-responsive grid2">
									<table class="table table-hover tab table-striped"
										id="tableData">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Offer Heading</th>
												<th>Status</th>
												<th>Offer Details</th>

											</tr>

										</thead>
										<c:forEach items="${offerList}" var="offerList"
											varStatus="status">
											<tr align="center">
												<td>${status.index+ 1 }</td>
												<td>${offerList.offerHeading}</td>
												<c:if test="${offerList.isApproved==1}">
													<td><form:checkbox path="isApproved"
															id="checkboxId${offerList.id}" value="${offerList.id}"
															checked="checked" onclick="enableOfferStatus(this);" /></td>
												</c:if>
												<c:if test="${offerList.isApproved==0}">
													<td><form:checkbox path="isApproved"
															id="checkboxId${offerList.id}" value="${offerList.id}"
															onclick="enableOfferStatus(this);" /></td>
												</c:if>
												<c:if test="${offerList.isApproved==2}">
													<td>Expired</td>
							</c:if>
											<c:if test="${offerList.isApproved==2}">
													<td>View Offer Details</td>
												</c:if>
												<c:if test="${offerList.isApproved==0}">
													<td><a href="#"
														onclick="return convertIdIntoBase64(${offerList['id']});">View
															Offer Details </a></td>
												</c:if>
												<c:if test="${offerList.isApproved==1}">
													<td><a href="#"
														onclick="return convertIdIntoBase64(${offerList['id']});">View
															Offer Details </a></td>
												</c:if>
											</tr>
										</c:forEach>
									</table>

								</div>
							</c:if>
							<c:if test="${empty offerList}">
								<h1 align="center" style="color: red">No Records Found</h1>
							</c:if>
							<div class="row"></div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
	</form:form>
</body>
</html>