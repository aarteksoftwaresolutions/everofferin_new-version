<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://cdn.jsdelivr.net/fontawesome/4.3.0/css/font-awesome.min.css">
<script src="http://code.jquery.com/jquery-2.1.1.js"></script>
<link href="resources/css/timepicker.css" rel="stylesheet"
	type="text/css" media="all" />
<script src="resources/js/timepicker.js"></script>
<link rel="stylesheet" href="resources/css/validationEngine.jquery.css"
	type="text/css" />
<script src="resources/js/jquery.validationEngine-en.js"
	type="text/javascript" charset="utf-8"></script>
<script src="resources/js/jquery.validationEngine.js"
	type="text/javascript" charset="utf-8"></script>
<script type="text/javascript"
	src="resources/js/commonForFormValidation.js"></script>
<script type="text/javascript">
	function validate() {
		var imgVal = $('#uploadBtn').val();
		var oFile = $('#uploadBtn')[0].files[0];

		// hide all errors
		$('#file_error').hide();

		// check for image type (jpg and png are allowed)
		var rfilter = /^(image\/jpeg|image\/png)$/i;

		if (!rfilter.test(oFile.type)) {
			$('#file_error')
					.html(
							'Please select a valid image file (jpg and png are allowed)')
					.show();
			return false;
		}

		$("#file_error").html("");
		var file_size = $('#uploadBtn')[0].files[0].size;
		if (file_size > 2097152) {
			$("#file_error").css("display", "block");
			$("#file_error").html("Please select image upto 2 MB");
			return false;
		}
		return true;
	}
</script>
</head>
<body>

	<div id="page-wrapper">

		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-8">
					<div id="admincontainer">

						<div>
							<h3 align="center" class="hadding">Edit Offer Details</h3>
						</div>
						<form:form method="POST" action="saveOffer" id="formID"
							modelAttribute="Offer" enctype="multipart/form-data"
							autocomplete="off" onSubmit="return validate();">
							<form:hidden path="id" />
							<form:hidden path="createdDate" />
							<form:hidden path="isApproved" />
							<form:hidden path="offerCoupanCode" />
							<form:hidden path="image" />
						<form:hidden path="loginId" value="${Offer.login.id}" />
							<div class="panel-body">
								<div class="forpass_sec form-sec">
									<div class="col-sm-12 frmpadding box">
										<div class="col-lg-12 col-sm-12 ">
											<label>Offer Heading</label>
											<form:input path="offerHeading" maxlength="250"
												class="validate[required] input-text user form-control"
												placeholder="Offer Heading" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12 col-sm-12 ">
											<label>Offer Description</label>
											<form:textarea path="offerDescription" maxlength="250"
												rows="4" class="validate[required] input-text"
												placeholder="Offer Description" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>Normal Price</label>
											<form:input path="normalPriceT" placeholder="Normal Price"
												name="normalPrice" class="validate[custom[integer]]"
												maxlength="8" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>Offer Price</label>
											<form:input path="offerPriceT" placeholder="Offer Price"
												name="offerPrice" class="validate[custom[integer]]"
												maxlength="8" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label class="Form-label--tick"> <form:radiobutton
													path="rateType" value="1" name="SomeRadio"
													class="Form-label-radio" /> <span class="Form-label-text">Discount</span>
											</label> <label class="Form-label--tick"> <form:radiobutton
													path="rateType" value="2" name="SomeRadio"
													class="Form-label-radio" /> <span class="Form-label-text">Flat</span>
											</label>

										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12 col-sm-12 ">
											<form:input path="rateValue" maxlength="8"
												class="validate[required,custom[integer]]"
												placeholder="Discount" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-6">
											<label>Offer Start Time</label>
											<form:input path="offerStartTime"
												class="timepicker validate[required]"
												placeholder="Offer Start Time" name="offerStartTime" />
											<script type="text/javascript">
												$('.timepicker').wickedpicker({
													twentyFour : false
												});
												$('.timepicker-two')
														.wickedpicker({
															twentyFour : true
														});
											</script>
										</div>
										<div class="col-lg-6">
											<label>Offer End time</label>
											<form:input path="offerEndTime" name="offerEndTime"
												class="timepicker validate[required]"
												placeholder="Offer End time" />
											<script type="text/javascript">
												$('.timepicker').wickedpicker({
													twentyFour : false
												});
												$('.timepicker-two')
														.wickedpicker({
															twentyFour : true
														});
											</script>
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>Offer Validity</label>
											<form:input path="offerValidity" type="date" value="${Offer.offerValidity}"
												name="offerValidity" placeholder="YYYY-MM-DD"
												class="validate[required,custom[date],future[NOW]]"
												maxlength="10" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>Terms & Conditions</label>
											<form:textarea path="termsAndConditions" rows="4"
												placeholder="Terms & Conditions" name="termsAndConditions"
												class="validate[required] input-text" maxlength="250" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>No. of Coupons / Person</label>
											<form:input path="noOfCouponsPerPerson"
												placeholder="No. of Coupons / Person"
												name="noOfCouponsPerPerson"
												class="validate[required,custom[integer]]" maxlength="1" />
										</div>
										<div style="clear: both;"></div>
										<div class="col-lg-12">
											<label>Upload Image</label><br> <span id="file_error"
												style="color: red; font-size: 15px; font-style: italic; display: none;"></span>
											<input id="uploadFile" placeholder="Choose File"
												disabled="disabled" />
											<div class="fileUpload btn btn-primary">
												<span>Upload</span><input id="uploadBtn" type="file"
													name="upload" class="upload" accept="image/*" />
											</div>
										</div>
										<div style="clear: both;"></div>
										<div class=" col-sm-7 col-md-7 col-lg-7 padd0">
											<div class=" col-sm-3 col-md-3 col-lg-3">
												<label class="hvr-sweep-to-right adOButt"> <input
													type="submit" value="Save">
												</label>
											</div>

											<!-- <div class="col-sm-3 col-md-3 col-lg-3">

												Trigger the modal with a button
												<button type="button" class="btn btn-info btn-lg"
													data-toggle="modal" data-target="#myModal">Preview
													offer</button>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	document.getElementById("uploadBtn").onchange = function() {
		document.getElementById("uploadFile").value = this.value;
	};
</script>
</html>