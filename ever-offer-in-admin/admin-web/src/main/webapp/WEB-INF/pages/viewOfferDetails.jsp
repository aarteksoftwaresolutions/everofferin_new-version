<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
	function goBack() {
		window.history.back();
	}
</script>
<script src="resources/js/page-js/base64.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript">
        function convertIdIntoBase64(id) {
            var encodedId = Base64.encode(id.toString());
            document.location.href = "editoffer.do?id="+encodedId;
        }
</script>
</head>
<body>
	<div id="page-wrapper">

		<div class="container-fluid">

			<!-- Page Heading -->
			<div class="row grid2"></div>
			<!-- /.row -->

			<div class="row">
				<div class="col-lg-6">
					<div id="admincontainer">
						<div class="container">

							<div class="content_userinfo col-lg-6">
								<div class="col-sm-8">
									<h4 style="font-size: 26px;">Offer Details</h4>
								</div>
								<div class="grid1">
									<div class="form-sec">
										<div class="row grid2">
											<div class="col-sm-12">
												<c:if test="${not empty massage}">
													<div class="alert alert-info">${massage}</div>
												</c:if>

											</div>

										</div>
										<hr>
										<div class="row">
											<div class="col-sm-4">
												<a class="story-img" href="#"><img
													src="resources/images/Logo.png"
													style="width: 100px; height: 100px" class="img-circle"></a>
											</div>
											<div class="col-md-4"></div>
										</div>
										<div class="row grid2">
											<div class="col-sm-4">
												<p>Offer Heading:</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.offerHeading}</strong>
												</p>
											</div>
											<div class="col-sm-4">
												<p>Offer Description:</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.offerDescription}</strong>
												</p>
											</div>
											<div class="col-sm-4">
												<p>Offer Validity</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.offerValidity}</strong>
												</p>
											</div>


										</div>

										<div class="row">
											<div class="col-sm-4">
												<p>offer Coupan Code</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.offerCoupanCode}</strong>
												</p>
											</div>
											<div class="col-sm-4">
												<p>Terms And Conditions</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.termsAndConditions}</strong>
												</p>
											</div>

										</div>

										<div class="row">
											<div class="col-sm-4">
												<p>No Of Coupons Per Person</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.noOfCouponsPerPerson}</strong>
												</p>
											</div>
											<div class="col-sm-4">
												<p>Normal Price</p>
											</div>
											<div class="col-sm-8">
												<c:if test="${offer.normalPrice==0}">
													<p>
														<strong>Not applied</strong>
													</p>
												</c:if>
												<c:if test="${offer.normalPrice!=0}">
													<p>
														<strong>${offer.normalPrice}</strong>
													</p>
												</c:if>
											</div>

										</div>

										<div class="row">
											<div class="col-sm-4">
												<p>Offer Price</p>
											</div>
											<div class="col-sm-8">
												<c:if test="${offer.offerPrice==0}">
													<p>
														<strong>Not applied</strong>
													</p>
												</c:if>
												<c:if test="${offer.offerPrice!=0}">
													<p>
														<strong>${offer.offerPrice}</strong>
													</p>
												</c:if>
											</div>

										</div>
										<div class="row">
											<div class="col-sm-4">
												<p>Rate Type</p>
											</div>
											<div class="col-sm-8">
												<c:if test="${offer.rateType==1}">
													<p>
														<strong>Dicount</strong>
													</p>
												</c:if>
												<c:if test="${offer.rateType==2}">
													<p>
														<strong>Flat</strong>
													</p>
												</c:if>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<p>Rate Value</p>
											</div>
											<div class="col-sm-8">
												<p>
													<strong>${offer.rateValue}</strong>
												</p>
											</div>

										</div>
										<div class="row">
											<div class="col-sm-2 frmpadding">

												<input type="submit" value="Back" onclick="goBack()" />
											</div>
											<div class="col-sm-2 frmpadding">
												<p>
													<input type="button" value="Edit" class="edit"
														onclick="return convertIdIntoBase64(${offer['id']});" />
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container-fluid -->

	</div>
</body>
</html>