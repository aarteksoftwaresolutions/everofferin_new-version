<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="resources/js/page-js/viewOfferList.js"></script>
<link rel="stylesheet" href="resources/css/validationEngine.jquery.css" type="text/css" />
<script src="resources/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="resources/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="resources/js/commonForFormValidation.js"></script>
<script type="text/javascript">
	function ifCityNotSelected(field, rules, i, options) {
		var b = document.getElementById('city').value;
		if (b == 0) {
			return "*Please select an option, this field is required";
		}
	}
</script>
</head>
<body>
    <div id="page-wrapper">
        <div class="container-fluid">
            <form:form method="POST" action="saveAdvertisement" id="formID" modelAttribute="Advertisement"
                enctype="multipart/form-data" autocomplete="off">
                <div class="row">
                    <div class="col-lg-8">
                        <div id="admincontainer">
                            <div>
                                <h3 align="center" class="hadding">Add Advertisement</h3>
                            </div>
                            <div class="panel-body">
                                <c:if test="${empty massage}">
                                    <div class="alert alert-info">Please fill details to add a new Advertisement</div>
                                </c:if>
                                <c:if test="${not empty massage}">
                                    <div class="alert alert-info">${massage}</div>
                                </c:if>

                                <div class="forpass_sec form-sec">
                                    <div class="col-sm-12 frmpadding box">
                                        <div class="col-lg-12 col-sm-12 ">

                                            <label>Name</label>
                                            <form:input path="name" maxlength="60" placeholder="Name"
                                                class="validate[required] input-text user form-control" />
                                        </div>
                                        <div class="col-lg-12 col-sm-12 frmpadding">

                                            <label>Select City</label>
                                            <form:select path="city.id"
                                                class="dropdown custarrow validate[funcCall[ifCityNotSelected]],dropdown"
                                                tabindex="9" data-settings="{&quot;wrapperClass&quot;:&quot;flat&quot;}"
                                                id="city">
                                                <option value="0">Select City</option>
                                                <c:forEach items="${cityList}" var="city">
                                                    <option value="${city.id}">${city.cityName}</option>
                                                </c:forEach>
                                            </form:select>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div class="col-lg-12 col-sm-12">
                                            <label>Upload Image</label><br> <span id="file_error"
                                                style="color: red; font-size: 15px; font-style: italic; display: none;"></span>
                                            <input id="uploadFile" placeholder="Choose File" disabled="disabled" />
                                            <div class="fileUpload btn btn-primary">
                                                <span>Upload</span><input id="uploadBtn" type="file" name="upload"
                                                    class="upload" accept="image/*" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3 frmpadding">
                                            <input type="submit" value="Submit" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 26px" class="table-responsive">
                    <table border="1" style="width: 100%; height: 26px;" id="tableData"
                        class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Status</th>
                                <th>Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="view" items="${advertisementList}" varStatus="status">
                                <tr>
                                    <td>${status.index+ 1}</td>
                                    <td>${view.name}</td>
                                    <td>${view.city.cityName}</td>
                                    <td><c:if test="${view.isApproved==1}">
                                            <form:checkbox path="isApproved" id="checkboxId${view.id}"
                                                value="${view.id}" checked="checked" onclick="enableAdStatus(this);" />
                                        </c:if> <c:if test="${view.isApproved==0}">
                                            <form:checkbox path="isApproved" id="checkboxId${view.id}"
                                                value="${view.id}" onclick="enableAdStatus(this);" />
                                        </c:if></td>
                                    <td><img src="http://everoffer.in/advImage/${view.image}" width="50px"
                                        height="50px"></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${empty advertisementList}">
                        <h1 align="left" class="msg" style="margin-bottom: 10px;">No Records Found</h1>
                    </c:if>
                </div>
            </form:form>
        </div>
    </div>
</body>
<script>
	document.getElementById("uploadBtn").onchange = function() {
		document.getElementById("uploadFile").value = this.value;
	};
</script>
</html>