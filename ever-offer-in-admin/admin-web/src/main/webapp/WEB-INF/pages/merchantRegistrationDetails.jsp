<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function goBack() {
    window.history.back();
}</script>
</head>
<body>
	<form:form align="center" method="POST" modelAttribute="Login">
		<div id="page-wrapper">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row grid2"></div>
				<!-- /.row -->
				<div class="row">
					<div id="admincontainer" class="col-lg-12">
						<div class="col-lg-12">
							<c:if test="${not empty registrationDetails}">
								<h1 class="text-left">Registration Details</h1>
								<div class="table-responsive grid2">
									<table class="table table-hover tab table-striped"
										id="tableData">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Merchants</th>
												<th>Merchant Type</th>
												<th>Subscription Plan/Complementary</th>
												<th>Amount</th>

											</tr>
										</thead>
										<c:forEach items="${registrationDetails}" var="merchantList"
											varStatus="status">
											<tr align="center">
												<td>${status.index+ 1 }</td>
												<td>${merchantList.name}</td>
												<c:if test="${merchantList.merchantType==1}">
													<td>Small merchant</td>
												</c:if>
												<c:if test="${merchantList.merchantType==2}">
													<td>Large merchant</td>
												</c:if>
												<c:if test="${not empty merchantList.subscriptionPlan}">
													<c:if test="${merchantList.subscriptionPlan==1}">
														<td>Thirty day subscription</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==2}">
														<td>3 months subscription</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==3}">
														<td>6 months subscription</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==4}">
														<td>1 year subscription
														<td>
													</c:if>
												</c:if>
												<c:if test="${empty merchantList.subscriptionPlan}">
													<td>Complementary</td>
												</c:if>
												<c:if test="${merchantList.merchantType==1}">
													<c:if test="${merchantList.subscriptionPlan==1}">
														<td>400</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==2}">
														<td>900</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==3}">
														<td>1500</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==4}">
													2500
													</c:if>
												</c:if>
												<c:if test="${merchantList.merchantType==2}">
													<c:if test="${merchantList.subscriptionPlan==1}">
														<td>650</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==2}">
														<td>1500</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==3}">
														<td>2000</td>
													</c:if>
													<c:if test="${merchantList.subscriptionPlan==4}">
													4000
													</c:if>
												</c:if>
											</tr>
										</c:forEach>
										</tbody>
									</table>
								</div>
							</c:if>
							<c:if test="${empty registrationDetails}">
								<h1 align="center" style="color: red">No Records Found</h1>
							</c:if>
								<div class="row">
											<div class="col-sm-2 frmpadding">
											<input type="submit" value="Back" class="back" onclick="goBack()"/>
										</div>
										</div>
						</div>
					</div>
				</div>

				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
	</form:form>
</body>
</html>