<%@ page isELIgnored="false" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form:form align="center" method="POST" modelAttribute="Login">
		<div id="page-wrapper">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row grid2"></div>
				<!-- /.row -->
				<div class="row">
					<div id="admincontainer" class="col-lg-12">
						<div class="col-lg-12">
							<c:if test="${not empty ComplementaryCode}">
								<h1 class="text-left">Complementary code</h1>
								<div class="table-responsive grid2">
									<table class="table table-hover tab table-striped"
										id="tableData">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Complementary Code</th>
												<th>Status</th>
											</tr>
										</thead>
										<c:forEach items="${ComplementaryCode}" var="list"
											varStatus="status">
											<tr align="center">
												<td>${status.index+ 1 }</td>
												<td>${list.complementaryCode}</a></td>
												<c:if test="${list.status==1}">
													<td>active</a></td>
												</c:if>
												<c:if test="${list.status==0}">
													<td>inActive</a></td>
												</c:if>
											</tr>
										</c:forEach>

										</tbody>
									</table>

								</div>
							</c:if>
							<c:if test="${empty ComplementaryCode}">
								<h1 align="center" style="color: red">No Records Found</h1>
							</c:if>
							<div class="row"></div>
						</div>
					</div>
				</div>

				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
	</form:form>
</body>
</html>