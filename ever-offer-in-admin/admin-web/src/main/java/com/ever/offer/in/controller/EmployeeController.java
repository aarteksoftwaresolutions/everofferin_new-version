package com.ever.offer.in.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ever.offer.in.model.Employee;
import com.ever.offer.in.model.Supervisor;
import com.ever.offer.in.service.EmployeeService;
import com.ever.offer.in.service.SupervisorService;
import com.ever.offer.in.util.IConstant;

@Controller
@SuppressWarnings("unused")
public class EmployeeController {

    private static final Logger logger = Logger.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private SupervisorService supervisorService;

    /**
     * Show employee registration page and get all supervisor.
     * 
     * @param map
     * @param massage
     * @param model
     * @return
     */
    @RequestMapping("/registration")
    public String showEmployeeRegistration(Map<String, Object> map, String massage, ModelMap model) {
        model.addAttribute("employeeList", employeeService.getAllEmployee());
        model.addAttribute("supervisorList", supervisorService.getAllSupervisor());
        model.addAttribute("massage", massage);
        map.put("Employee", new Employee());
        return "employeeRegistration";
    }

    /**
     * Employee registration.
     * 
     * @param employee
     * @param model
     * @return
     */
    @RequestMapping(value = "/saveEmployee", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("Employee") Employee employee, ModelMap model) {
        employeeService.saveEmployee(employee);
        model.addAttribute("massage", IConstant.EMPLOYEE);
        return "redirect:/registration";

    }

    /**
     * Edit employee information.
     * 
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/editEmployee")
    public String editEmployee(ModelMap model, @RequestParam("id") String id) {
        byte[] decodedId = Base64.decodeBase64(id.getBytes());
        model.addAttribute("Employee", employeeService.editEmployee(Integer.parseInt(new String(decodedId))));
        model.addAttribute("employeeList", employeeService.getAllEmployee());
        model.addAttribute("supervisorList", supervisorService.getAllSupervisor());
        return "employeeRegistration";
    }

    /**
     * Employee soft delete.
     * 
     * @param model
     * @param id
     * @param employee
     * @return
     */
    @RequestMapping(value = "/deleteEmployee")
    public String deleteEmployee(ModelMap model, @RequestParam(required = false) Integer id,
                    @ModelAttribute Employee employee) {
        employeeService.deleteEmployee(id);
        return "redirect:/registration";
    }
}
