package com.ever.offer.in.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ever.offer.in.model.Employee;
import com.ever.offer.in.model.Login;
import com.ever.offer.in.service.EmployeeService;
import com.ever.offer.in.service.GenerateReportService;

@Controller
@SuppressWarnings("unused")
public class GenerateReportController {

    private static final Logger logger = Logger.getLogger(GenerateReportController.class);

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private GenerateReportService generateReportService;

    /**
     * Get employee list.
     * 
     * @param map
     * 
     * 
     * @return
     */
    @RequestMapping(value = "/generateReport", method = { RequestMethod.GET, RequestMethod.POST })
    public String generateReport(Map<String, Object> map) {
        map.put("employee", new Employee());
        map.put("employeeList", employeeService.getAllEmployee());
        return "employeeReport";
    }

    /**
     * Get registration count value by the refer employee.
     * 
     * @param employeeId
     * @param startDate
     * @param endDate
     * @return
     */
    @RequestMapping(value = "/getEmployeeReport", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public List<Login> getEmployeeReport(@RequestParam Integer employeeId, String startDate, String endDate) {
        return generateReportService.getEmployeeReportService(employeeId, startDate, endDate);
    }

    /**
     * Get merchants registration details by field officer.
     * 
     * @param employeeId
     * @param model
     * @return
     */
    @RequestMapping(value = "/employeeDetails", method = { RequestMethod.GET, RequestMethod.POST })
    public String getEmployeeRegistrationDetails(@RequestParam String employeeId, ModelMap model) {
        byte[] decodedId = Base64.decodeBase64(employeeId.getBytes());
        model.addAttribute("registrationDetails",
                        generateReportService.getEmployeeRegistrationDetails(Integer.parseInt(new String(decodedId))));
        return "merchantRegistrationDetails";
    }
}