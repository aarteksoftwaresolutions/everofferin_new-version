package com.ever.offer.in.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ever.offer.in.model.Login;
import com.ever.offer.in.service.AdminService;
import com.ever.offer.in.service.LoginService;

/**
 * 
 * @author Meenal Pathre
 *
 */
@Controller
@SuppressWarnings("unused")
public class AdminHomeController {

	private static final Logger logger = Logger.getLogger(AdminHomeController.class);

	@Autowired
	private AdminService adminService;

	@Autowired
	private LoginService loginService;

	/**
	 * show welcome page
	 */

	@RequestMapping("/welcome")
	private String showWelcomePage(ModelMap model) {
		return "welcome";
	}

	/**
	 * view merchant list
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/merchantList")
	private String showMerchantList(ModelMap model) {
		model.addAttribute("merchantList", adminService.getMerchantList());
		return "merchantList";
	}

	/**
	 * Update merchant's location.
	 * 
	 * @param merchantId
	 * @param lat
	 * @param lang
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateLatLang", method = RequestMethod.GET)
	public String updateLatLong(@RequestParam(required = false) String lat, String lang, Integer merchantId) {
		return loginService.updateLatLang(lat, lang, merchantId);

	}

	/**
	 * Show error page
	 * 
	 * @return
	 */
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@RequestMapping(value = "/*")
	private String Error() {
		return "error";
	}
}
