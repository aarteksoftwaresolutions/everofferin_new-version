package com.ever.offer.in.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ever.offer.in.model.SubAdmin;
import com.ever.offer.in.service.SubAdminService;
import com.ever.offer.in.util.IConstant;

@Controller
public class SubAdminController {

    @Autowired
    private SubAdminService subAdminService;

    /**
     * Show sub admin registration page and show all registered sub admin.
     * 
     * @param map
     * @return
     */
    @RequestMapping(value = "/subAdminRegistration")
    public String showRegistrationPage(Map<String, Object> map, ModelMap model) {
        List<SubAdmin> subAdminList = subAdminService.getAllSubAdmin();
        subAdminList.remove(IConstant.ZERO);
        model.addAttribute("subAdminList", subAdminList);
        map.put("SubAdmin", new SubAdmin());
        return "subAdminRegistration";
    }

    /**
     * Sub admin registration.
     * 
     * @param subAdmin
     * @param model
     * @return
     */
    @RequestMapping(value = "/saveSubadmin", method = RequestMethod.POST)
    public String saveSubadmin(@ModelAttribute("subAdmin") SubAdmin subAdmin, ModelMap model) {
        subAdminService.saveSubadmin(subAdmin);
        model.addAttribute("massage", IConstant.SUB_ADMIN);
        return "redirect:/subAdminRegistration";
    }

}
