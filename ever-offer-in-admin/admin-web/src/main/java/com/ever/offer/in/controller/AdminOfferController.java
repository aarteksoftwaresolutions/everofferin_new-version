package com.ever.offer.in.controller;

import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Offer;
import com.ever.offer.in.service.OfferService;
import com.ever.offer.in.util.IConstant;

/**
 * 
 * @author Meenal Pathre
 *
 */
@Controller
@SuppressWarnings("unused")
public class AdminOfferController {

	private static final Logger logger = Logger.getLogger(AdminOfferController.class);

	@Autowired
	private OfferService offerService;

	/**
	 * show the offerDetailsList
	 * 
	 * @param offerId
	 * @param model
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/offerDetailsList", method = { RequestMethod.GET, RequestMethod.POST })
	public String showOfferDetailsList(@RequestParam String id, ModelMap model, Map<String, Object> map, String massage) {
		if (id.matches("[0-9]+")) {
			model.addAttribute("offer", offerService.getOffer(Integer.parseInt(new String(id))));
		} else {
			byte[] decodedOfferId = Base64.decodeBase64(id.getBytes());
			model.addAttribute("offer", offerService.getOffer(Integer.parseInt(new String(decodedOfferId))));
		}
		map.put("Offer", new Offer());
		map.put("massage", massage);
		return "viewDetailsList";
	}

	/**
	 * show the offer list
	 * 
	 * @param offerId
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/offerList", method = { RequestMethod.GET, RequestMethod.POST })
	public String showOfferList(@RequestParam(required = false) String id, Map<String, Object> map) {
		byte[] decodeOfferId = Base64.decodeBase64(id.getBytes());
		map.put("offerList", offerService.getOfferList(Integer.parseInt(new String(decodeOfferId))));
		map.put("Offer", new Offer());
		return "viewOfferList";
	}

	/**
	 * check the offer status
	 * 
	 * @param offerId
	 * @param statusValue
	 */
	@RequestMapping(value = "/offerStatus", method = RequestMethod.GET)
	@ResponseBody
	public void changeOfferStatus(@RequestParam(required = false) String statusIdValue, Integer statusValue) {
		offerService.changeOfferStatus(statusIdValue, statusValue);
	}

	/**
	 * Save Edited offer.
	 * 
	 * @param offer
	 * @param model
	 * @param id
	 * @param upload
	 * @return
	 */
	@RequestMapping(value = "/saveOffer", method = RequestMethod.POST)
	public String saveOffer(@ModelAttribute("Offer") Offer offer, ModelMap model, @RequestParam("id") Integer id,
					@RequestParam CommonsMultipartFile[] upload) {
		offerService.saveOffer(offer, upload);
		model.addAttribute("id", offer.getId());
		model.addAttribute("massage", IConstant.UPDATE_DETAILS);
		return "redirect:/offerDetailsList";
	}

	/**
	 * Get offer information for edit offer.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/editoffer")
	public String editOfferDetails(@RequestParam("id") String id, Map<String, Object> map) {
		byte[] decodedId = Base64.decodeBase64(id.getBytes());
		map.put("Offer", offerService.editOffer((Integer.parseInt(new String(decodedId)))));
		return "addOffer";
	}

}
