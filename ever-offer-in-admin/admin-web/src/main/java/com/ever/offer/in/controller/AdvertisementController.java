package com.ever.offer.in.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Advertisement;
import com.ever.offer.in.model.City;
import com.ever.offer.in.service.AdvertisementService;
import com.ever.offer.in.util.IConstant;

@Controller
@SuppressWarnings("unused")
public class AdvertisementController {

	private static final Logger logger = Logger.getLogger(Advertisement.class);

	@Autowired
	private AdvertisementService advertisementService;

	/**
	 * Show advertisement page and it's details.
	 * 
	 * @param map
	 * @param massage
	 * @param model
	 * @return
	 */
	@RequestMapping("/advertisement")
	public String showEmployeeRegistration(Map<String, Object> map, String massage, ModelMap model) {
		map.put("Advertisement", new Advertisement());
		model.addAttribute("cityList", advertisementService.getAllCities());
		model.addAttribute("advertisementList", advertisementService.getAllAdvertisement());
		map.put("massage", massage);
		return "addAdvertisement";
	}

	/**
	 * Save advertisement details.
	 * 
	 * @param advertisement
	 * @param map
	 * @param upload
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveAdvertisement", method = RequestMethod.POST)
	public String saveAdvertisement(@ModelAttribute("Advertisement") Advertisement advertisement, ModelMap map,
					@RequestParam CommonsMultipartFile[] upload) throws Exception {
		City city = advertisementService.getCityName(advertisement.getCity().getId());
		advertisementService.saveAdvertisement(advertisement, upload, city.getCityName());
		map.addAttribute("massage", IConstant.ADVERTISEMENT);
		return "redirect:/advertisement";
	}

	/**
	 * Change advertisement status.
	 * 
	 * @param statusIdValue
	 * @param statusValue
	 */
	@RequestMapping(value = "changeAdStatus", method = RequestMethod.GET)
	@ResponseBody
	public void changeAdStatus(@RequestParam(required = false) String statusIdValue, Integer statusValue) {
		advertisementService.changeAdStatus(statusIdValue, statusValue);
	}

}
