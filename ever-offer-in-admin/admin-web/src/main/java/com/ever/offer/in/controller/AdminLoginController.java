package com.ever.offer.in.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ever.offer.in.model.SubAdmin;
import com.ever.offer.in.service.LoginService;
import com.ever.offer.in.util.IConstant;

/**
 * 
 * @author Meenal Pathre
 *
 */
@Controller
@SuppressWarnings("unused")
public class AdminLoginController {

    private static final Logger logger = Logger.getLogger(AdminLoginController.class);

    @Autowired
    private LoginService loginService;

    /**
     * Show login page.
     * 
     * @param map
     * @param invalid
     * @return
     */

    @RequestMapping("/login")
    private String showloginPage(Map<String, Object> map, String invalid) {
        map.put("SubAdmin", new SubAdmin());
        map.put("invalid", invalid);
        return "login";
    }

    /**
     * check the userName and password
     * 
     * @param login
     * @param model
     * @param map
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public String login(@ModelAttribute("subAdmin") SubAdmin subAdmin, ModelMap model, HttpServletRequest request,
                    HttpServletResponse response) {
        HttpSession session = request.getSession();
        subAdmin = loginService.adminSignIn(subAdmin);
        if (null == subAdmin) {
            model.addAttribute("invalid", IConstant.INVALID_LOGIN_MESSAGE);
            return "redirect:/login";
        } else {
            session.setAttribute("subAdmin", subAdmin);
            return "redirect:/welcome";
        }
    }

    /**
     * Admin logged out.
     * 
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        session.invalidate();
        model.addAttribute("invalid", IConstant.LOGOUT_SUCCESSFUL);
        return "redirect:/login";
    }

}
