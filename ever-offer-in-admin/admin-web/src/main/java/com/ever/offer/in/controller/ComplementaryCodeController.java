package com.ever.offer.in.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ever.offer.in.model.ComplementaryCode;
import com.ever.offer.in.service.ComplementaryCodeService;

@Controller
@SuppressWarnings("unused")
public class ComplementaryCodeController {

	private static final Logger logger = Logger.getLogger(ComplementaryCodeController.class);

	@Autowired
	private ComplementaryCodeService complementaryCodeService;

	/**
	 * show the Complementary Code List
	 * 
	 * @param complementaryCode
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/generateComplementaryCode", method = { RequestMethod.GET, RequestMethod.POST })
	public String generateComplementaryCode(@ModelAttribute("ComplementaryCode") ComplementaryCode complementaryCode,
					ModelMap model) {
		model.addAttribute("ComplementaryCode", complementaryCodeService.getComplementaryCodeList());
		return "randamList";
	}
}
