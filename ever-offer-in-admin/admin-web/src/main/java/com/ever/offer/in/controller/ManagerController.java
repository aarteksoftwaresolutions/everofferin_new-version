package com.ever.offer.in.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ever.offer.in.model.ManagerRegistration;
import com.ever.offer.in.service.ManagerService;
import com.ever.offer.in.util.IConstant;

@Controller
@SuppressWarnings("unused")
public class ManagerController {

    private static final Logger logger = Logger.getLogger(ManagerController.class);

	@Autowired
	private ManagerService managerService;

	/**
	 * Show Manager Registration page.
	 * 
	 * @param map
	 * @param massage
	 * @param model
	 * @return
	 */
	@RequestMapping("/manager")
	public String showManagerRegistration(Map<String, Object> map, String massage, ModelMap model) {
		model.addAttribute("massage", massage);
		map.put("ManagerRegistration", new ManagerRegistration());
		model.addAttribute("managerList", managerService.getAllManager());
		return "managerRegistration";
	}

	/**
	 * Manager registration.
	 * 
	 * @param manager
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saveManager", method = RequestMethod.POST)
	public String saveManager(@ModelAttribute("ManagerRegstration") ManagerRegistration manager, ModelMap model) {
		managerService.saveManger(manager);
		model.addAttribute("massage", IConstant.MANAGER);
		return "redirect:/manager";

	}

	/**
	 * Edit manager information.
	 * 
	 * @param model
	 * @param managerId
	 * @param managerRegistration
	 * @return
	 */
	@RequestMapping(value = "/editManager", method = RequestMethod.GET)
	public String editManager(ModelMap model, @RequestParam(required = false) String id) {
		byte[] decodedId = Base64.decodeBase64(id.getBytes());
		model.addAttribute("ManagerRegistration", managerService.editManager(Integer.parseInt(new String(decodedId))));
		model.addAttribute("managerList", managerService.getAllManager());
		return "managerRegistration";
	}

	/**
	 * Soft delete manager.
	 * 
	 * @param model
	 * @param managerId
	 * @param managerRegistration
	 * @return
	 */
	@RequestMapping(value = "/deleteManager")
	public String deleteManager(ModelMap model, @RequestParam(required = false) Integer id,
					@ModelAttribute ManagerRegistration managerRegistration) {
		managerService.deleteManager(id);
		return "redirect:/manager";
	}

}
