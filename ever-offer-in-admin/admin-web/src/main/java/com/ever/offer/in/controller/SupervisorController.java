package com.ever.offer.in.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ever.offer.in.model.ManagerRegistration;
import com.ever.offer.in.model.Supervisor;
import com.ever.offer.in.service.ManagerService;
import com.ever.offer.in.service.SupervisorService;
import com.ever.offer.in.util.IConstant;

@Controller
@SuppressWarnings("unused")
public class SupervisorController {

    private static final Logger logger = Logger.getLogger(SupervisorController.class);

    @Autowired
    private SupervisorService supervisorService;

    @Autowired
    private ManagerService managerService;

    /**
     * Show supervisor registration page and fetch list of all manager.
     * 
     * @param map
     * @param massage
     * @param model
     * @return
     */
    @RequestMapping("/supervisor")
    public String showSupervisorRegistration(Map<String, Object> map, String massage, ModelMap model) {
        model.addAttribute("managerList", managerService.getAllManager());
        map.put("Supervisor", new Supervisor());
        model.addAttribute("massage", massage);
        model.addAttribute("supervisorList", supervisorService.getAllSupervisor());
        return "supervisorRegistration";

    }

    /**
     * Supervisor registration.
     * 
     * @param supervisor
     * @param model
     * @return
     */
    @RequestMapping(value = "/saveSupervisor", method = RequestMethod.POST)
    public String saveManager(@ModelAttribute("Supervisor") Supervisor supervisor, ModelMap model) {
        supervisorService.saveManger(supervisor);
        model.addAttribute("massage", IConstant.SUPERVISOR);
        return "redirect:/supervisor";

    }

    /**
     * Edit supervisor information.
     * 
     * @param model
     * @param id
     * @param supervisor
     * @return
     */
    @RequestMapping(value = "/editSupervisor")
    public String editSupervisor(ModelMap model, @RequestParam("id") String id) {
        byte[] decodedId = Base64.decodeBase64(id.getBytes());
        model.addAttribute("Supervisor", supervisorService.editSupervisor(Integer.parseInt(new String(decodedId))));
        model.addAttribute("managerList", managerService.getAllManager());
        model.addAttribute("supervisorList", supervisorService.getAllSupervisor());
        return "supervisorRegistration";
    }

    /**
     * Supervisor soft delete
     * 
     * @param model
     * @param supervisorId
     * @param supervisor
     * @return
     */
    @RequestMapping(value = "/deleteSupervisor")
    public String deleteSupervisor(ModelMap model, @RequestParam(required = false) Integer id,
                    @ModelAttribute Supervisor supervisor) {
        supervisorService.deleteSupervisor(id);
        return "redirect:/supervisor";
    }

}
