package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.SubAdmin;

public interface SubAdminService {

	void saveSubadmin(SubAdmin subAdmin);

	List<SubAdmin> getAllSubAdmin();

}
