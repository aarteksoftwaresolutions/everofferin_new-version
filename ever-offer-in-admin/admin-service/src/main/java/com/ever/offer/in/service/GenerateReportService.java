package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.Login;

public interface GenerateReportService {
	public List<Login> getEmployeeReportService(int employeeId, String startDate, String endDate);

	public List<Login> getEmployeeRegistrationDetails(Integer employeeId);
}
