package com.ever.offer.in.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ever.offer.in.model.Employee;
import com.ever.offer.in.repository.EmployeeRepository;
import com.ever.offer.in.service.EmployeeService;
import com.ever.offer.in.util.IConstant;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Save employee information.
     * 
     */
    public void saveEmployee(Employee employee) {
        employee.setCreatedDate(new Date().toString());
        employee.setIsDeleted(IConstant.IS_DELETED);
        employeeRepository.save(employee);
    }

    /**
     * Get all employee
     * 
     */
    public List<Employee> getAllEmployee() {
        return employeeRepository.findByIsDeleted(IConstant.IS_DELETED);
    }

    /**
     * Change employee is deleted.
     * 
     */
    public void deleteEmployee(Integer employeeId) {
        Employee employee = employeeRepository.findOne(employeeId);
        employee.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
        employeeRepository.save(employee);

    }

    /**
     * Get information for edit employee information.
     * 
     */
    public Employee editEmployee(Integer employeeId) {
        return employeeRepository.findOne(employeeId);
    }
}
