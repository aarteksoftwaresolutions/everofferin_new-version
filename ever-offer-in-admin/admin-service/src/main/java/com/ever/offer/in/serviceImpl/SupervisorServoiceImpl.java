package com.ever.offer.in.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ever.offer.in.model.Supervisor;
import com.ever.offer.in.repository.SupervisorRepository;
import com.ever.offer.in.service.SupervisorService;
import com.ever.offer.in.util.IConstant;

@Service
public class SupervisorServoiceImpl implements SupervisorService {

    @Autowired
    private SupervisorRepository supervisorRepository;

    /**
     * Save supervisor information.
     * 
     */
    public void saveManger(Supervisor supervisor) {
        supervisor.setCreatedDate(new Date().toString());
        supervisor.setIsDeleted(IConstant.IS_DELETED);
        supervisorRepository.save(supervisor);

    }

    /**
     * Get all supervisor.
     * 
     */
    public List<Supervisor> getAllSupervisor() {
        return supervisorRepository.findByIsDeleted(IConstant.IS_DELETED);
    }

    /**
     * Change supervisor Status
     * 
     */
    public void deleteSupervisor(Integer id) {
        Supervisor supervisor = supervisorRepository.findOne(id);
        supervisor.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
        supervisorRepository.save(supervisor);
    }

    /**
     * Get information for edit supervisor information.
     * 
     */
    public Supervisor editSupervisor(Integer id) {
        return supervisorRepository.findOne(id);
    }
}
