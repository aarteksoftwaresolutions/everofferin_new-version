package com.ever.offer.in.service;

import com.ever.offer.in.model.SubAdmin;

public interface LoginService {

	public SubAdmin adminSignIn(SubAdmin subAdmin);

	public String updateLatLang(String lat, String lang, Integer merchantId);

}
