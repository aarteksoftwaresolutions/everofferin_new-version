package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.Supervisor;

public interface SupervisorService {

	void saveManger(Supervisor supervisor);

	public List<Supervisor> getAllSupervisor();

	void deleteSupervisor(Integer id);

	Supervisor editSupervisor(Integer id);

}
