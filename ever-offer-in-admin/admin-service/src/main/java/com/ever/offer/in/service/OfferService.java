package com.ever.offer.in.service;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Offer;

public interface OfferService {

	public List<Offer> getOfferList(Integer id);

	public void changeOfferStatus(String offerId, Integer statusValue);

	public Offer getOffer(Integer id);

	public Offer editOffer(Integer id);

	public void saveOffer(Offer offer, CommonsMultipartFile[] upload);

}
