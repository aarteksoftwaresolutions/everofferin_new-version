package com.ever.offer.in.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Advertisement;
import com.ever.offer.in.model.City;
import com.ever.offer.in.repository.AdvertisementRepository;
import com.ever.offer.in.repository.CityRepository;
import com.ever.offer.in.service.AdvertisementService;
import com.ever.offer.in.util.DateFormatUtil;
import com.ever.offer.in.util.IConstant;
import com.ever.offer.in.util.ImageUploadUtils;
import com.ever.offer.in.util.LatLng;

@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private CityRepository cityRepository;

    public List<City> getAllCities() {
        return cityRepository.findAll(new Sort(Sort.Direction.ASC, "cityName"));
    }

    public void saveAdvertisement(Advertisement advertisement, CommonsMultipartFile[] upload, String CityName)
                    throws Exception {
        String imageName = ImageUploadUtils.getImage(upload);
        String latLongs[] = LatLng.getLatLongPositions(CityName);
        advertisement.setImage(imageName);

        advertisement.setLetitude(latLongs[IConstant.ZERO]);
        advertisement.setLongitude(latLongs[IConstant.ONE]);
        advertisement.setCreatedDate(DateFormatUtil.getDate());
        advertisement.setUpdatedDate(DateFormatUtil.getDate());
        advertisement.setIsApproved(IConstant.NOT_APPROVED);

        advertisementRepository.save(advertisement);
    }

    public City getCityName(Integer cityId) {
        return cityRepository.findOne(cityId);
    }

    public List<Advertisement> getAllAdvertisement() {
        return advertisementRepository.findAll();
    }

    public void changeAdStatus(String statusValue, Integer advertisementId) {
        if (null != advertisementId && null != statusValue) {
            Advertisement advertisement = advertisementRepository.findOne(advertisementId);
            if (statusValue.equals("true")) {
                advertisement.setIsApproved(IConstant.IS_APPROVED);
                advertisementRepository.save(advertisement);
            } else {
                advertisement.setIsApproved(IConstant.NOT_APPROVED);
                advertisementRepository.save(advertisement);
            }
        }
    }
}
