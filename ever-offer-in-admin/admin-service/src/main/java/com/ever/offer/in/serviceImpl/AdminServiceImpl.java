package com.ever.offer.in.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ever.offer.in.model.Login;
import com.ever.offer.in.repository.AdminRepository;
import com.ever.offer.in.service.AdminService;
import com.ever.offer.in.util.IConstant;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminRepository adminRepository;

    /**
     * view the list of merchant
     */
    public List<Login> getMerchantList() {
        return adminRepository.findByRoleId(IConstant.MERCHANT_ROLE_ID);
    }
}
