package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.ComplementaryCode;

public interface ComplementaryCodeService {

	/* public void saveComplementaryCode(ComplementaryCode complementaryCode); */

	public List<ComplementaryCode> getComplementaryCodeList();

}
