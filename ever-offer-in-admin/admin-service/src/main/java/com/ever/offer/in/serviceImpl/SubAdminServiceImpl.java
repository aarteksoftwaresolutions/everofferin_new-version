package com.ever.offer.in.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ever.offer.in.model.SubAdmin;
import com.ever.offer.in.repository.SubAdminRepository;
import com.ever.offer.in.service.SubAdminService;

@Service
@Transactional
public class SubAdminServiceImpl implements SubAdminService {

    @Autowired
    private SubAdminRepository subAdminRepository;

    public void saveSubadmin(SubAdmin subAdmin) {
        subAdmin.setCreatedDate(new Date().toString());
        subAdminRepository.save(subAdmin);
    }

    public List<SubAdmin> getAllSubAdmin() {
        return subAdminRepository.findAll();
    }
}
