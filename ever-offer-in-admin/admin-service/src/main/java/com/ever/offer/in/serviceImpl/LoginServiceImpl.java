package com.ever.offer.in.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ever.offer.in.model.Login;
import com.ever.offer.in.model.SubAdmin;
import com.ever.offer.in.repository.LoginRepository;
import com.ever.offer.in.repository.SubAdminRepository;
import com.ever.offer.in.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private SubAdminRepository subAdminRepository;

	@Autowired
	private LoginRepository loginRepository;

	/**
	 * compare the admin emailId and password.
	 * 
	 */
	public SubAdmin adminSignIn(SubAdmin subAdmin) {
		return subAdminRepository.findByUserNameAndPassword(subAdmin.getUserName(), subAdmin.getPassword());
	}

	/**
	 * Update merchant's lat long.
	 * 
	 */
	public String updateLatLang(String lat, String lang, Integer merchantId) {
		Login login = loginRepository.findOne(merchantId);
		login.getAddress().setLetitude(lat);
		login.getAddress().setLongitude(lang);
		loginRepository.save(login);
		return null;
	}
}
