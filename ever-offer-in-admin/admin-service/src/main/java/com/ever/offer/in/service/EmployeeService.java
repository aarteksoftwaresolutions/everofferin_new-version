package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.Employee;

public interface EmployeeService {

	void saveEmployee(Employee employee);

	List<Employee> getAllEmployee();

	void deleteEmployee(Integer id);

	Employee editEmployee(Integer id);

}
