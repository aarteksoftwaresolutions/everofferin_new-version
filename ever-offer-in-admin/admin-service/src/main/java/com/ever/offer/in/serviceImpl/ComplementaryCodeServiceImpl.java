package com.ever.offer.in.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ever.offer.in.model.ComplementaryCode;
import com.ever.offer.in.repository.ComplementaryCodeRepository;
import com.ever.offer.in.service.ComplementaryCodeService;

@Service
public class ComplementaryCodeServiceImpl implements ComplementaryCodeService {

    @Autowired
    private ComplementaryCodeRepository complementaryCodeRepository;

    /**
     * show the Complementary Code list
     */
    public List<ComplementaryCode> getComplementaryCodeList() {
        return complementaryCodeRepository.findAll();
    }
}
