package com.ever.offer.in.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ever.offer.in.model.Login;
import com.ever.offer.in.repository.LoginRepository;
import com.ever.offer.in.service.GenerateReportService;
import com.ever.offer.in.util.ConvertDate;
import com.ever.offer.in.util.IConstant;

@Service
public class GenerateReportServiceImpl implements GenerateReportService {

    @Autowired
    private LoginRepository loginRepository;

    /**
     * Generate employee report based on start date and end date
     */
    public List<Login> getEmployeeReportService(int employeeId, String startDate, String endDate) {
        List<Login> employeeList = loginRepository.getEmployeeReportService(employeeId,
                        ConvertDate.getConvertedDate(startDate), ConvertDate.getConvertedDate(endDate));
        List<Login> amountList = new ArrayList<Login>();
        List<Login> finalList = new ArrayList<Login>();
        if (null != employeeList && !employeeList.isEmpty()) {
            int count = IConstant.ZERO;
            int amount = IConstant.ZERO;
            for (Login login : employeeList) {
                count = count + IConstant.ONE;
                if (login.getSubscriptionPlan() != null) {
                    if (login.getMerchantType() == IConstant.ONE) {
                        if (login.getSubscriptionPlan() == IConstant.THIRTY_DAYS_SUBSCRIPTION) {
                            amount = amount + IConstant.THIRTY_DAYS_SUBSCRIPTION_SMALL_AMOUNT;
                        } else if (login.getSubscriptionPlan() == IConstant.THREE_MONTH_SUBSCRIPTION) {
                            amount = amount + IConstant.THREE_MONTH_SUBSCRIPTION_SMALL_AMOUNT;
                        } else if (login.getSubscriptionPlan() == IConstant.SIX_MONTHS_SUBSCRIPTION) {
                            amount = amount + IConstant.SIX_MONTHS_SUBSCRIPTION_SMALL_AMOUNT;
                        } else if (login.getSubscriptionPlan() == IConstant.ONE_YEAR_SUBSCRIPTION) {
                            amount = amount + IConstant.ONE_YEAR_SUBSCRIPTION_SMALL_AMOUNT;
                        }
                    } else {
                        if (login.getMerchantType() == IConstant.TWO) {
                            if (login.getSubscriptionPlan() == IConstant.THIRTY_DAYS_SUBSCRIPTION) {
                                amount = amount + IConstant.THIRTY_DAYS_SUBSCRIPTION_COMBO_AMOUNT;
                            } else if (login.getSubscriptionPlan() == IConstant.THREE_MONTH_SUBSCRIPTION) {
                                amount = amount + IConstant.THREE_MONTH_SUBSCRIPTION_COMBO_AMOUNT;
                            } else if (login.getSubscriptionPlan() == IConstant.SIX_MONTHS_SUBSCRIPTION) {
                                amount = amount + IConstant.SIX_MONTHS_SUBSCRIPTION_COMBO_AMOUNT;
                            } else if (login.getSubscriptionPlan() == IConstant.ONE_YEAR_SUBSCRIPTION) {
                                amount = amount + IConstant.ONE_YEAR_SUBSCRIPTION_COMBO_AMOUNT;
                            }
                        }
                    }
                }
                login.setCount(count);
                login.setAmount(amount);
                login.setFieldOfficer(login.getEmployee().getName());

                login.setSupervisior(login.getEmployee().getSupervisor().getName());
                login.setManager(login.getEmployee().getSupervisor().getManagerRegstration().getName());

                login.setEmpAddress(login.getEmployee().getAddress());
                login.setEmailId(login.getEmployee().getEmail());
                amountList.add(login);
            }
            finalList.add(amountList.get(amountList.size() - IConstant.ONE));
            return finalList;
        } else {
            return employeeList;
        }
    }

    /**
     * Get employee based on employeeId
     */
    public List<Login> getEmployeeRegistrationDetails(Integer employeeId) {
        return loginRepository.findByEmployeeId(employeeId);
    }
}
