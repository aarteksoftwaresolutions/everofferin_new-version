package com.ever.offer.in.service;

import java.util.List;

import com.ever.offer.in.model.ManagerRegistration;

public interface ManagerService {

	void saveManger(ManagerRegistration manager);

	public List<ManagerRegistration> getAllManager();

	void deleteManager(Integer id);

	ManagerRegistration editManager(Integer id);

}
