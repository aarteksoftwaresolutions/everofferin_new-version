package com.ever.offer.in.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Login;
import com.ever.offer.in.model.Offer;
import com.ever.offer.in.repository.OfferRepository;
import com.ever.offer.in.service.OfferService;
import com.ever.offer.in.util.IConstant;
import com.ever.offer.in.util.ImageUploadUtils;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepository offerRepository;

    /**
     * show the offer list
     */
    public List<Offer> getOfferList(Integer loginId) {
        return offerRepository.getOfferList(loginId);
    }

    /**
     * change the offer status
     */
    public void changeOfferStatus(String offerId, Integer statusValue) {
        if (null != offerId && null != statusValue) {
            Offer offer = offerRepository.findOne(statusValue);
            if (offerId.equals("true")) {
                offer.setIsApproved(IConstant.IS_APPROVED);
                offerRepository.save(offer);
            } else {
                offer.setIsApproved(IConstant.NOT_APPROVED);
                offerRepository.save(offer);
            }
        }
    }

    /**
     * show the offer list
     */
    public Offer getOffer(Integer offerId) {
        return offerRepository.findOne(offerId);
    }

    /**
     * Update offer
     */
    public Offer editOffer(Integer offerId) {
        Offer offer = offerRepository.findOne(offerId);
        if (IConstant.ZERO == offer.getNormalPrice()) {
            offer.setNormalPriceT(null);
        } else {
            offer.setNormalPriceT(Integer.toString(offer.getNormalPrice()));
        }
        if (IConstant.ZERO == offer.getOfferPrice()) {
            offer.setOfferPriceT(null);
        } else {
            offer.setOfferPriceT(Integer.toString(offer.getOfferPrice()));
        }
        return offer;
    }

    /**
     * Save offer
     */
    public void saveOffer(Offer offer, CommonsMultipartFile[] upload) {
        if (null != offer.getNormalPriceT() && !offer.getNormalPriceT().isEmpty()) {
            offer.setNormalPrice(Integer.parseInt(offer.getNormalPriceT()));
        } else {
            offer.setNormalPrice(IConstant.ZERO);
        }

        if (null != offer.getOfferPriceT() && !offer.getOfferPriceT().isEmpty()) {
            offer.setOfferPrice(Integer.parseInt(offer.getOfferPriceT()));
        } else {
            offer.setOfferPrice(IConstant.ZERO);
        }
        for (CommonsMultipartFile multipartFile : upload) {
            if (multipartFile.getSize() != IConstant.ZERO) {
                String imageName = ImageUploadUtils.getOfferImage(upload);
                offer.setImage(imageName);
            }
        }
        Login login = new Login();
        login.setId(offer.getLoginId());
        offer.setLogin(login);
        offerRepository.save(offer);
    }
}
