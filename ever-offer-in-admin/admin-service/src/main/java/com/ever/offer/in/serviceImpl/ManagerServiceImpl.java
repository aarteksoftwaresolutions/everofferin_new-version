package com.ever.offer.in.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ever.offer.in.model.ManagerRegistration;
import com.ever.offer.in.repository.ManagerRepository;
import com.ever.offer.in.service.ManagerService;
import com.ever.offer.in.util.IConstant;

@Service
@Transactional
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    /**
     * Save manager.
     * 
     */
    public void saveManger(ManagerRegistration manager) {
        manager.setCreatedDate(new Date().toString());
        manager.setIsDeleted(IConstant.IS_DELETED);
        managerRepository.save(manager);

    }

    /**
     * Get all Manager.
     * 
     */
    public List<ManagerRegistration> getAllManager() {
        return managerRepository.findByIsDeleted(IConstant.IS_DELETED);
    }

    /**
     * Change manager is deleted.
     * 
     */
    public void deleteManager(Integer managerId) {
        ManagerRegistration managerRegistration = (ManagerRegistration) managerRepository.findOne(managerId);
        managerRegistration.setIsDeleted(IConstant.IS_DELETED_DEACTIVE);
        managerRepository.save(managerRegistration);

    }

    /**
     * Get manager information for edit manager.
     * 
     */
    public ManagerRegistration editManager(Integer managerId) {
        return managerRepository.findOne(managerId);
    }

}
