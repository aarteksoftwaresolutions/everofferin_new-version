package com.ever.offer.in.service;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ever.offer.in.model.Advertisement;
import com.ever.offer.in.model.City;

public interface AdvertisementService {

	public List<City> getAllCities();

	public void saveAdvertisement(Advertisement advertisement, CommonsMultipartFile[] upload, String CityName)
					throws Exception;

	public City getCityName(Integer cityId);

	public List<Advertisement> getAllAdvertisement();

	public void changeAdStatus(String statusValue, Integer advertisementId);

}
